// -*- C++ -*-

#include <cassert>

#include <vector>

#include "element_allocator.h"

namespace fumi {

ElementAllocator::ElementAllocator(IResizer& resizer_) : resizer(&resizer_) {
  int usedSize = resizer->size();

  int size = defaultSize;
  while (size <= resizer->size()) { size *= 2; }

  table.resize(size, static_cast<int>(usedValue));
  resizer->resize(size);

  for (int i = usedSize; i < size - 1; i++) {
    table[i] = i + 1;
  }
  table[size - 1] = lastValue;

  freeIndex = usedSize;
  numAllocated = usedSize;
}

int
ElementAllocator::alloc() {
  assert(freeIndex != usedValue);
  int res = freeIndex;
  freeIndex = table[freeIndex];
  table[res] = usedValue;

  if (freeIndex == lastValue) {
    int oldSize = table.size();
    int newSize = oldSize * 2;

    table.resize(newSize);
    resizer->resize(newSize);

    for (int i = oldSize; i < newSize - 1; i++) {
      table[i] = i + 1;
    }
    table[newSize - 1] = lastValue;
    freeIndex = oldSize;
  }

  ++numAllocated;

  assert(table[freeIndex] != usedValue && res != usedValue);
  return res;
}

void
ElementAllocator::free(int index) {
  assert(table[index] == usedValue);
  table[index] = freeIndex;
  freeIndex = index;
  --numAllocated;
  assert(table[index] != usedValue);
}

bool
ElementAllocator::isUsed(int index) {
  return table[index] == static_cast<int>(usedValue);
}

int
ElementAllocator::getNumAllocated() {
  return numAllocated;
}

}
