// -*- C++ -*-

#ifndef FUMI_GLDRIVER_H_
#define FUMI_GLDRIVER_H_

#if defined(FUMI_USE_QT)

# include <QOpenGLFunctions_3_3_Core>

namespace fumi {
namespace gldriver {

extern QOpenGLFunctions_3_3_Core* glFuncs;

static void
initGLDriver(QOpenGLFunctions_3_3_Core* glFuncs_) {
  glFuncs = glFuncs_;
}

}
}

# define FUMI_CALLGL(f) ::fumi::gldriver::glFuncs->f

#else
#error "Cannot detect opengl function provider"
#endif

#endif // FUMI_GLDRIVER_H_
