// -*- C++ -*-

#ifndef FUMI_MESH_MANIPULATION_H_
#define FUMI_MESH_MANIPULATION_H_

#include "sculptable_mesh.h"
#include "brush.h"

namespace fumi {

int intersection(const SculptableMesh& mesh, const glm::vec3& orig, const glm::vec3& dir, glm::vec3& res, glm::vec3& normal);
std::vector<int> chooseVertices(SculptableMesh& mesh, const glm::vec3& orig, const glm::vec3& dir, float radius);
void chooseVerticesAux(SculptableMesh& mesh, std::set<int>& candidates, std::set<int>& traversed, int vertex, std::vector<int>& res);
std::vector<int> chooseFace(SculptableMesh& mesh, const glm::vec3& orig, const glm::vec3& dir, float radius);
void sculpt(SculptableMesh& mesh, IBrush& brush, const glm::vec3& orig, const glm::vec3& dir, float radius);
void divide(SculptableMesh& mesh, const glm::vec3& orig, const glm::vec3& dir, float radius, float maxLength, float maxLengthIncreaseFactor);
void symmetrize(SculptableMesh& mesh);

}

#endif // FUMI_MESH_MANIPULATION_H_
