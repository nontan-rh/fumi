// -*- C++ -*-

#ifndef FUMI_IMESH_LOADER_H_
#define FUMI_IMESH_LOADER_H_

namespace fumi {

class IMeshLoader {
public:
  virtual bool load() = 0;
  virtual std::vector<glm::vec3> getVertices() = 0;
  virtual std::vector<Face> getFaces() = 0;
  virtual std::vector<glm::vec3> getNormals() = 0;
  virtual std::vector<glm::vec3> getColors() = 0;
  virtual int numFaces() = 0;
  virtual int numVertices() = 0;
};

}

#endif // FUMI_IMESH_LOADER_H_
