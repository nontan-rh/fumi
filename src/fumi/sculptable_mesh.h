// -*- C++ -*-

#ifndef FUMI_SCULPTABLE_MESH_H_
#define FUMI_SCULPTABLE_MESH_H_

#include <vector>
#include <set>

#include <glm/glm.hpp>

#include "common.h"
#include "buffered_vector.h"
#include "element_allocator.h"
#include "imesh_loader.h"

namespace fumi {

class SculptableMesh {
  SculptableMesh();
  SculptableMesh(SculptableMesh& m);
  SculptableMesh& operator=(const SculptableMesh& m);

public:
  SculptableMesh(IMeshLoader& meshLoader);

  void draw();

  BufferedVector<Face> faces;
  BufferedVector<glm::vec3> vertices;
  BufferedVector<glm::vec3> normals;
  BufferedVector<glm::vec3> colors;
  std::vector<std::vector<int> > containedFaces;
  int numVertices;
  int numFaces;

  bool symmetrical;
  std::vector<int> symmetricalFace;
  std::vector<int> symmetricalVertex;

  std::unique_ptr<ElementAllocator> vertexAllocator;
  std::unique_ptr<ElementAllocator> faceAllocator;

  void calculateContainedFaces();
  void setSymmetrical(bool symmetrical);
  int getNumFaces();

private:
  class VertexVectorResizer : public ElementAllocator::IResizer {
  public:
    VertexVectorResizer(SculptableMesh& parent);
    virtual void resize(int size);
    virtual int size();
  private:
    SculptableMesh* parent;
  } vertexVectorResizer;

  class FaceVectorResizer : public ElementAllocator::IResizer {
  public:
    FaceVectorResizer(SculptableMesh& parent);
    virtual void resize(int size);
    virtual int size();
  private:
    SculptableMesh* parent;
  } faceVectorResizer;
};

}

#endif // FUMI_SCULPTABLE_MESH_H_
