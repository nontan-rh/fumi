// -*- C++ -*-

#include <glm/glm.hpp>

#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>

#include "common.h"
#include "geometry.h"

#include "obj_loader.h"

namespace fumi {

const int nullIndex = -1;

static Polygon
parseF(const std::vector<std::string>& tokens) {
  Polygon oFace;
  if (tokens.size() < 4) {
    throw std::runtime_error("Syntax error");
  }
  try {
    for (unsigned i = 1; i < tokens.size(); i++) {
      auto t = split(tokens[i], '/');
      if (t.size() == 1) { // Vertex only
        oFace.add(std::stoi(t[0]) - 1);
      } else if (t.size() == 3) { // Full
        int vt = t[1] != "" ? std::stoi(t[1]) - 1 : nullIndex;
        int vn = t[2] != "" ? std::stoi(t[2]) - 1 : nullIndex;
        oFace.add(std::stoi(t[0]) - 1, vt, vn);
      } else {
        throw std::runtime_error("Syntax error");
      }
    }
  } catch (std::invalid_argument& e) {
    throw std::runtime_error("Syntax error");
  } catch (std::out_of_range& e) {
    throw std::runtime_error("Syntax error");
  }
  return oFace;
}

static glm::vec3
parseV(const std::vector<std::string>& tokens) {
  if (tokens.size() != 4) {
    throw std::runtime_error("Syntax error");
  }
  glm::vec3 v;
  try {
    v.x = std::stof(tokens[1]);
    v.y = std::stof(tokens[2]);
    v.z = std::stof(tokens[3]);
  } catch (std::invalid_argument& e) {
    throw std::runtime_error("Syntax error");
  } catch (std::out_of_range& e) {
    throw std::runtime_error("Syntax error");
  }
  return v;
}

ObjLoader::ObjLoader(const std::string& filename)
  : filename_(filename), v(0), vn(0), Polygons(0) {
}

bool
ObjLoader::load() {
  std::ifstream f(filename_);

  if (f.fail()) {
    throw std::runtime_error("File not found");
  }

  std::string line;

  while (getline(f, line)) {
    if (line == "\r" || line[0] == '#') { // Null line or Comment
      continue;
    }

    auto tokens = split(line, ' ');
    if (tokens.size() == 0) { // Null line
      continue;
    }
    auto cmd = tokens[0];

    if (cmd == "v") {
      v.push_back(parseV(tokens));
    } else if (cmd == "vt") {
      // Ignore
    } else if (cmd == "vn") {
      // Ignore
    } else if (cmd == "f") {
      Polygons.push_back(parseF(tokens));
    } else if (cmd == "o") {
      // Ignore
    } else if (cmd == "mtllib") {
      // Ignore
    } else if (cmd == "usemtl") {
      // Ignore
    } else if (cmd == "s") {
      // Ignore
    } else {
      throw std::runtime_error("Unknown command");
    }
  }

  faces = dividePolygonArrayNaive(Polygons, v);

  calculateNormals();
  return true;
}

std::vector<glm::vec3>
ObjLoader::getVertices() {
  return v;
}

std::vector<Face>
ObjLoader::getFaces() {
  return faces;
}

std::vector<glm::vec3>
ObjLoader::getNormals() {
  return vn;
}

std::vector<glm::vec3>
ObjLoader::getColors() {
  return std::vector<glm::vec3>(v.size(), glm::vec3(0.7f, 0.7f, 0.7f));
}

void
ObjLoader::calculateNormals() {
  vn.resize(v.size());
  for (unsigned i = 0; i < faces.size(); i++) {
    int vi0 = faces[i].vertexIndices[0];
    int vi1 = faces[i].vertexIndices[1];
    int vi2 = faces[i].vertexIndices[2];
    glm::vec3 n = glm::normalize(glm::cross(v[vi1] - v[vi0], v[vi2] - v[vi0]));
    vn[vi0] += n;
    vn[vi1] += n;
    vn[vi2] += n;
  }
  for (unsigned i = 0; i < vn.size(); i++) {
    vn[i] = glm::normalize(vn[i]);
  }
}

int
ObjLoader::numFaces() {
  return faces.size();
}

int
ObjLoader::numVertices() {
  return v.size();
}

}
