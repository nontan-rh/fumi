// -*- C++ -*-

#ifndef FUMI_COMMON_H_
#define FUMI_COMMON_H_

#include <vector>
#include <memory>

// For GLfloat
#include "gldriver.h"

namespace fumi {

const double pi = 3.141592653589793;

static inline float
deg2Rad(float deg) {
  return deg / 180 * pi;
}

static inline float
rad2Deg(float rad) {
  return rad / pi * 180;
}

struct Color {
  GLfloat r;
  GLfloat g;
  GLfloat b;
  GLfloat a;

  Color() : r(0.0f), g(0.0f), b(0.0f), a(0.0f) { }
  Color(GLfloat r_, GLfloat g_, GLfloat b_, GLfloat a_)
    : r(r_), g(g_), b(b_), a(a_) { }
};

struct Material {
  GLfloat shininess;
  Color ambient;
  Color diffuse;
  Color specular;
  Color emission;
};

struct Light {
  Color ambient;
  Color diffuse;
  Color specular;
};

struct Face {
  GLuint vertexIndices[3];

  Face(const Face& f) {
    for (int i = 0; i < 3; i++) {
      vertexIndices[i] = f.vertexIndices[i];
    }
  }

  Face(int i, int j, int k) {
    vertexIndices[0] = i;
    vertexIndices[1] = j;
    vertexIndices[2] = k;
  }

  Face() {}
};

struct Edge {
  GLuint vertexIndices[2];

  Edge(int i, int j) {
    vertexIndices[0] = std::min(i, j);
    vertexIndices[1] = std::max(i, j);
  }

  bool operator<(const Edge& rht) const {
    const Edge& lht = *this;
    if (lht.vertexIndices[0] == rht.vertexIndices[0]) {
      return lht.vertexIndices[1] < rht.vertexIndices[1];
    }
    return lht.vertexIndices[0] < rht.vertexIndices[0];
  }

  bool operator==(const Edge& rht) const {
    const Edge& lht = *this;
    return (lht.vertexIndices[0] == rht.vertexIndices[0]
      && lht.vertexIndices[1] == rht.vertexIndices[1]);
  }
};

struct Polygon {
  std::vector<int> vIndices;
  std::vector<int> vtIndices;
  std::vector<int> vnIndices;

  void add(int v, int vt = -1, int vn = -1) {
    vIndices.push_back(v);
    vtIndices.push_back(vt);
    vnIndices.push_back(vn);
  }

  int numVertices() const {
    return vIndices.size();
  }
};

std::vector<std::string> split(const std::string &str, char delim);

template <class T>
static std::unique_ptr<T[]>
vector2RawArray(const std::vector<T>& v) {
  std::unique_ptr<T[]> res(new T[v.size()]);
  for (unsigned i = 0; i < v.size(); i++) {
    res[i] = v[i];
  }
  return res;
}

static inline float
clamp(float x, float min, float max) {
  return std::min(std::max(x, min), max);
}

}

#endif // FUMI_COMMON_H_
