// -*- C++ -*-

#include <QOpenGLFunctions_3_3_Core>

#include <glm/glm.hpp>

#include "common.h"
#include "imesh_loader.h"
#include "buffered_vector.h"
#include "gldriver.h"

#include "static_mesh.h"

namespace fumi {

StaticMesh::StaticMesh(IMeshLoader& meshLoader)
  : faces(GL_ELEMENT_ARRAY_BUFFER),
    vertices(GL_ARRAY_BUFFER),
    normals(GL_ARRAY_BUFFER),
    colors(GL_ARRAY_BUFFER) {
  meshLoader.load();

  faces.init(meshLoader.getFaces(), meshLoader.numFaces());
  vertices.init(meshLoader.getVertices(), meshLoader.numVertices());
  normals.init(meshLoader.getNormals(), meshLoader.numVertices());
  colors.init(meshLoader.getColors(), meshLoader.numVertices());
}

void
StaticMesh::draw() {
  FUMI_CALLGL(glEnableVertexAttribArray(0));
  FUMI_CALLGL(glEnableVertexAttribArray(1));
  FUMI_CALLGL(glEnableVertexAttribArray(2));

  vertices.vertexAttribPointer(0);
  normals.vertexAttribPointer(1);
  colors.vertexAttribPointer(2);
  faces.drawElementsTriangles(faces.size());

  FUMI_CALLGL(glDisableVertexAttribArray(0));
  FUMI_CALLGL(glDisableVertexAttribArray(1));
  FUMI_CALLGL(glDisableVertexAttribArray(2));
}

}
