// -*- C++ -*-

#ifndef FUMI_USERCONFIG_H_
#define FUMI_USERCONFIG_H_

#include <string>

#include "common.h"

namespace fumi {

extern const char *configFile;

class UserConfig {
  UserConfig();

public:
  static void loadConfig();

  static Color clearColor;

  static std::string vertexShaderPath;
  static std::string fragmentShaderPath;
  static std::string debugVertexShaderPath;
  static std::string debugFragmentShaderPath;
  static std::string solidFragmentShaderPath;

  static float manipPivotRotateFactor;
  static float manipNoPivotRotateFactor;
  static float manipMoveForwardFactor;
  static float manipMoveToFrontOfPointerDistance;

  static float algoPointerBackOnSelectionFactor;
};

}

#endif // FUMI_USERCONFIG_H_
