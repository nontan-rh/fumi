// -*- C++ -*-

#ifndef FUMI_OBJ_LOADER_H_
#define FUMI_OBJ_LOADER_H_

#include <memory>
#include <string>
#include <vector>

#include <glm/glm.hpp>

#include "imesh_loader.h"

namespace fumi {

class ObjLoader : public IMeshLoader {
  std::string filename_;

  std::vector<glm::vec3> v;
  std::vector<glm::vec3> vn;
  std::vector<Polygon> Polygons;
  std::vector<Face> faces;

  void calculateNormals();

  ObjLoader();

public:
  ObjLoader(const std::string& filename);

  // IMeshLoader implementation
  virtual bool load();
  virtual std::vector<glm::vec3> getVertices();
  virtual std::vector<Face> getFaces();
  virtual std::vector<glm::vec3> getNormals();
  virtual std::vector<glm::vec3> getColors();
  virtual int numFaces();
  virtual int numVertices();
};

}

#endif // FUMI_OBJ_LOADER_H_
