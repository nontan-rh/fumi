// -*- C++ -*-

#include <fstream>
#include <string>
#include <iterator>

#include "common.h"

#include "picojson.h"

#include "userconfig.h"

namespace fumi {

const char *configFile = "config.json";

static float
readColorElement(const std::string& col, int start) {
  // This function assumes col.length() == 7 & i equals one of 1, 3 and 5
  int vi = 0;
  for (int i = 0; i < 2; i++) {
    char c = col[start + i];
    if ('0' <= c && c <= '9') {
      vi = vi * 16 + (c - '0');
    } else if ('a' <= c && c <= 'f') {
      vi = vi * 16 + (c - 'a' + 10);
    } else if ('A' <= c && c <= 'F') {
      vi = vi * 16 + (c - 'A' + 10);
    } else {
      return -1.0f;
    }
  }
  return vi / 255.0f;
}

static Color
readColor(const std::string& col, const Color& defaultColor) {
    if (col.length() != 7 && col[0]== '#') {
      return defaultColor;
    }

    float r = readColorElement(col, 1);
    float g = readColorElement(col, 3);
    float b = readColorElement(col, 5);

    if (r < 0 || g < 0 || b < 0) {
      return defaultColor;
    }

    return Color(r, g, b, 1.0f);
}

template <typename T>
static T
getValue(picojson::value& v, const std::string& path, T defaultValue = T()) {
  std::vector<std::string> p = split(path, '.');
  if (!v.is<picojson::object>() || p.size() == 0) {
    return defaultValue;
  }

  picojson::object n = v.get<picojson::object>();

  for (unsigned i = 0; i < p.size() - 1; i++) {
    picojson::value nv = n[p[i]];
    if (!nv.is<picojson::object>()) {
      return defaultValue;
    }

    n = nv.get<picojson::object>();
  }

  picojson::value lv = n[p[p.size() - 1]];
  if (!lv.is<T>()) {
    return defaultValue;
  }

  return lv.get<T>();
}

void
UserConfig::loadConfig() {
  std::ifstream ifs(configFile);
  std::istream_iterator<char> ii(ifs);
  picojson::value v;
  std::string err;

  picojson::parse(v, ii, std::istream_iterator<char>(), &err);
  if (!err.empty()) {
      throw std::runtime_error(err);
  }

  // General
  clearColor = readColor(getValue<std::string>(v, "General.ClearColor"), Color());

  // Default light
  vertexShaderPath = getValue<std::string>(v, "Shader.VertexShaderPath");
  fragmentShaderPath = getValue<std::string>(v, "Shader.FragmentShaderPath");
  solidFragmentShaderPath = getValue<std::string>(v, "Shader.SolidFragmentShaderPath");

  debugVertexShaderPath = getValue<std::string>(v, "DebugShader.VertexShaderPath");
  debugFragmentShaderPath = getValue<std::string>(v, "DebugShader.FragmentShaderPath");

  manipPivotRotateFactor = getValue<double>(v, "Manipulation.PivotRotateSpeed");
  manipNoPivotRotateFactor = getValue<double>(v, "Manipulation.NoPivotRotateSpeed");
  manipMoveForwardFactor = getValue<double>(v, "Manipulation.MoveForwardSpeed");
  manipMoveToFrontOfPointerDistance = getValue<double>(v, "Manipulation.MoveToFrontOfPointerDistance");

  algoPointerBackOnSelectionFactor = getValue<double>(v, "Algorithm.PointerBackOnSelection");
}

Color UserConfig::clearColor;
std::string UserConfig::vertexShaderPath;
std::string UserConfig::fragmentShaderPath;
std::string UserConfig::debugVertexShaderPath;
std::string UserConfig::debugFragmentShaderPath;
std::string UserConfig::solidFragmentShaderPath;
float UserConfig::manipPivotRotateFactor;
float UserConfig::manipNoPivotRotateFactor;
float UserConfig::manipMoveForwardFactor;
float UserConfig::manipMoveToFrontOfPointerDistance;
float UserConfig::algoPointerBackOnSelectionFactor;

}
