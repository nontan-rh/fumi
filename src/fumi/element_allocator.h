// -*- C++ -*-

#ifndef FUMI_VERTEX_ALLOCATOR_H_
#define FUMI_VERTEX_ALLOCATOR_H_

#include <vector>

namespace fumi {

class ElementAllocator {
  static const int defaultSize = 100000;
  static const int usedValue = -1;
  static const int lastValue = -2;

  ElementAllocator();
  ElementAllocator(const ElementAllocator& va);
  ElementAllocator& operator=(const ElementAllocator& rht);

public:
  class IResizer {
  public:
    virtual void resize(int size) = 0;
    virtual int size() = 0;
  };

  ElementAllocator(IResizer& resizer);

  void init();

  int alloc();
  void free(int index);

  bool isUsed(int index);
  int getNumAllocated();

private:
  IResizer* resizer;

  int numAllocated;
  int freeIndex;
  std::vector<int> table;
};

}

#endif // FUMI_VERTEX_ALLOCATOR_H_
