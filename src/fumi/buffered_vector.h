// -*- C++ -*-

#ifndef FUMI_BUFFERED_VECTOR_H_
#define FUMI_BUFFERED_VECTOR_H_

#include <vector>
#include <set>
#include <algorithm>
#include <iostream>

#include "gldriver.h"

#include "modification_guard.h"

namespace fumi {

//
// BufferedVector is a vector-like container to improve the efficiency of VBOs.
//

template <typename T>
class BufferedVector {
  static const int maxNumModifiedElementsToRecord = 10000;
  static const int maxBufferSize = 10000;

  BufferedVector();
  BufferedVector(const BufferedVector& bvec);
	BufferedVector& operator=(const BufferedVector& bvec);

public:

  // You have to call init method if you use this constructor
  BufferedVector(GLenum target_)
    : vec(), modifiedIndices(), overflow(false), buffer(0), target(target_) {
    #ifdef SCULPTPROTO_DEBUG_BUILD
        modificationAllowed = false;
    #endif // SCULPTPROTO_DEBUG_BUILD
  }

  // You don't have to call init method if you use this constructor
  BufferedVector(
    const std::vector<T>& vec_,
    int size, GLenum target_)
    : vec(vec_), modifiedIndices(), overflow(false), target(target_) {

    vec.resize(size);
    FUMI_CALLGL(glGenBuffers(1, &buffer));

    FUMI_CALLGL(glBindBuffer(target, buffer));
    FUMI_CALLGL(glBufferData(target,
      size * sizeof(T), vec.data(), GL_STATIC_DRAW));
    FUMI_CALLGL(glBindBuffer(target, 0));

#ifdef SCULPTPROTO_DEBUG_BUILD
    modificationAllowed = false;
#endif // SCULPTPROTO_DEBUG_BUILD
  }

  ~BufferedVector() {
    FUMI_CALLGL(glDeleteBuffers(1, &buffer));
  }

  void init(const std::vector<T>& vec_, int size) {
    vec = vec_;
    vec.resize(size);

    FUMI_CALLGL(glGenBuffers(1, &buffer));

    FUMI_CALLGL(glBindBuffer(target, buffer));
    FUMI_CALLGL(glBufferData(target, size * sizeof(T), vec.data(), GL_STATIC_DRAW));
    FUMI_CALLGL(glBindBuffer(target, 0));
  }

  void beginModification() {
#ifdef SCULPTPROTO_DEBUG_BUILD
    modificationAllowed = true;
#endif // SCULPTPROTO_DEBUG_BUILD
  }

  void endModification() {
    FUMI_CALLGL(glBindBuffer(GL_ARRAY_BUFFER, buffer));
    if (overflow) {
      T* a = (T*)FUMI_CALLGL(glMapBuffer(GL_ARRAY_BUFFER, GL_WRITE_ONLY));
      for (int i = 0; i < size(); i++) {
        a[i] = vec[i];
      }
      FUMI_CALLGL(glUnmapBuffer(GL_ARRAY_BUFFER));
    } else {
      for (auto i = modifiedIndices.begin(); i != modifiedIndices.end();) {
        auto j = i;

        int firstIndex = *i;
        int lastIndex = *i;
        for (; j != modifiedIndices.end(); j++) {
          if (*j - firstIndex + 1 > maxBufferSize) {
            break;
          }
          lastIndex = *j;
        }

        T* a = (T*)FUMI_CALLGL(glMapBufferRange(
          GL_ARRAY_BUFFER,
          firstIndex * sizeof(T), (lastIndex - firstIndex + 1) * sizeof(T),
          GL_MAP_WRITE_BIT));

        for (; i != j; i++) {
          a[*i - firstIndex] = vec[*i];
        }
        FUMI_CALLGL(glUnmapBuffer(GL_ARRAY_BUFFER));
      }
    }
    FUMI_CALLGL(glBindBuffer(GL_ARRAY_BUFFER, 0));

    overflow = false;
    modifiedIndices.clear();
#ifdef SCULPTPROTO_DEBUG_BUILD
  modificationAllowed = false;
#endif // SCULPTPROTO_DEBUG_BUILD
  }

  int size() const {
    return static_cast<int>(vec.size());
  }

  const T& get(int i) const {
    return (const T&)vec[i];
  }

  const T& operator[](int i) const {
    return get(i);
  }

  void set(int i, const T& v) {
#ifdef SCULPTPROTO_DEBUG_BUILD
    if (!modificationAllowed) {
      throw std::logic_error("Modification is not allowed");
    }
#endif // SCULPTPROTO_DEBUG_BUILD
    if (overflow || modifiedIndices.size() > maxNumModifiedElementsToRecord) {
      overflow = true;
      modifiedIndices.clear();
    } else {
      modifiedIndices.insert(i);
    }
    vec[i] = v;
  }

  void resize(int size) {
    int oldSize = static_cast<int>(vec.size());
    vec.resize(size);

    GLuint newBuffer;
    FUMI_CALLGL(glGenBuffers(1, &newBuffer));
    FUMI_CALLGL(glBindBuffer(target, newBuffer));
    FUMI_CALLGL(glBufferData(target, size * sizeof(T), vec.data(), GL_STATIC_DRAW));

    FUMI_CALLGL(glDeleteBuffers(1, &buffer));
    buffer = newBuffer;

    FUMI_CALLGL(glBindBuffer(target, 0));
  }

  void vertexAttribPointer(
    GLuint index,
    GLint size = 3,
    GLenum type = GL_FLOAT) {

    FUMI_CALLGL(glBindBuffer(target, buffer));
    FUMI_CALLGL(glVertexAttribPointer(index, size, type, GL_FALSE, 0, 0));
    FUMI_CALLGL(glBindBuffer(target, 0));
  }

  void drawElementsTriangles(int numFaces, GLenum type = GL_UNSIGNED_INT) {
    FUMI_CALLGL(glBindBuffer(target, buffer));
    FUMI_CALLGL(glDrawElements(GL_TRIANGLES, numFaces * 3, type, 0));
    FUMI_CALLGL(glBindBuffer(target, 0));
  }

private:
  std::vector<T> vec;

  std::set<int> modifiedIndices;
  bool overflow;

  GLuint buffer;
  GLenum target;

#ifdef SCULPTPROTO_DEBUG_BUILD
  bool modificationAllowed;
#endif // SCULPTPROTO_DEBUG_BUILD
};

}

#endif // FUMI_BUFFERED_VECTOR_H_
