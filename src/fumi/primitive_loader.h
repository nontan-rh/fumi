// -*- C++ -*-

#ifndef FUMI_PRIMITIVE_LOADER_H_
#define FUMI_PRIMITIVE_LOADER_H_

#include <memory>

#include <glm/glm.hpp>

#include "common.h"
#include "imesh_loader.h"

namespace fumi {

class PrimitiveLoader : public IMeshLoader {
protected:
  std::vector<glm::vec3> vertices;
  std::vector<glm::vec3> normals;
  std::vector<Polygon> polygons;
  std::vector<Face> faces;

public:
  virtual std::vector<glm::vec3> getVertices();
  virtual std::vector<Face> getFaces();
  virtual std::vector<glm::vec3> getNormals();
  virtual std::vector<glm::vec3> getColors();
  virtual int numFaces();
  virtual int numVertices();
};

class TorusLoader : public PrimitiveLoader {
  TorusLoader();

  float innerRadius;
  float outerRadius;
  int sides;
  int rings;

public:
  TorusLoader(float innerRadius, float outerRadius, int sides, int rings);

  virtual bool load();
};

class SphereLoader : public PrimitiveLoader {
  SphereLoader();

  float radius;
  int division;
public:
  SphereLoader(float radius, int division);

  virtual bool load();
};

class CylinderLoader : public PrimitiveLoader {
  CylinderLoader();

  float radius1;
  float radius2;
  float height;
  int slices;
  int stacks;
public:
  CylinderLoader(float radius1, float radius2, float height, int slices, int stacks);

  virtual bool load();
};

class AnnulusLoader : public PrimitiveLoader {
  AnnulusLoader();

  float innerRadius;
  float outerRadius;
  int sides;

public:
  AnnulusLoader(float innerRadius, float outerRadius, int sides);

  virtual bool load();
};

}

#endif // FUMI_PRIMITIVE_LOADER_H_
