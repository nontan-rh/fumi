// -*- C++ -*-

#ifndef FUMI_STATIC_MESH_H_
#define FUMI_STATIC_MESH_H_

#include <glm/glm.hpp>

#include "common.h"
#include "imesh_loader.h"
#include "buffered_vector.h"

namespace fumi {

class StaticMesh {
  StaticMesh();
  StaticMesh(StaticMesh& m);
  StaticMesh& operator=(const StaticMesh& m);

public:
  StaticMesh(IMeshLoader& meshLoader);

  void draw();

  BufferedVector<Face> faces;
  BufferedVector<glm::vec3> vertices;
  BufferedVector<glm::vec3> normals;
  BufferedVector<glm::vec3> colors;
};

}

#endif
