// -*- C++ -*-

#include <iostream>
#include <algorithm>

#include <cmath>
#include <cfloat>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <ncpplib/math/ops.h>

#include "common.h"
#include "geometry.h"

namespace fumi {

glm::vec3
proj2WorldDirection(const CameraInfo& ci, float x, float y) {
  glm::mat4 projMatrix = glm::perspective(
    ci.fovY, ci.aspectRatio, ci.zNear, ci.zFar);
  glm::mat4 viewMatrix = glm::lookAt(
    ci.cameraPosition, ci.lookAtPosition, ci.upDirection);
  glm::vec3 v1 = glm::unProject(
    glm::vec3(x, ci.viewportHeight - y, 1.0f),
    glm::mat4(1),
    projMatrix * viewMatrix,
    glm::vec4(0, 0, ci.viewportWidth, ci.viewportHeight)
  );

  glm::vec3 v0 = glm::unProject(
    glm::vec3(x, ci.viewportHeight - y, 0.5f),
    glm::mat4(1),
    projMatrix * viewMatrix,
    glm::vec4(0, 0, ci.viewportWidth, ci.viewportHeight)
  );
  return v1 - v0;
}

void
getUnitVectorCSinWS(const CameraInfo& ci, glm::vec3& exWS, glm::vec3& eyWS, glm::vec3& ezWS) {
  ezWS = glm::normalize(ci.lookAtPosition - ci.cameraPosition);
  exWS = glm::normalize(glm::cross(ci.upDirection, ezWS));
  eyWS = glm::normalize(glm::cross(ezWS, exWS));
}

glm::vec3
getRotationAxisWS(const CameraInfo& ci, float dxPS, float dyPS) {
  glm::vec3 exWS, eyWS, ezWS;
  getUnitVectorCSinWS(ci, exWS, eyWS, ezWS);
  return glm::cross(dxPS * exWS + dyPS * eyWS, ezWS);
}

glm::mat4
getRotationMatrix(const glm::vec3& from, const glm::vec3& to) {
  glm::vec3 axis = glm::cross(from, to);
  if (ncpplib::math::isZero(glm::length(axis))) {
    return glm::mat4(1);
  }
  glm::vec3 f = glm::normalize(from);
  glm::vec3 t = glm::normalize(to);
  float angle = std::acos(clamp(glm::dot(f, t), -1.0f, 1.0f));
  return glm::rotate(glm::mat4(1), angle, axis);
}

CameraInfo
moveCameraAroundPivot(
  const CameraInfo& ci, const glm::vec3& pivotWS,
  float angle, const glm::vec3& axisWS) {

  glm::vec3 d = glm::normalize(ci.lookAtPosition - ci.cameraPosition);
  glm::vec3 p = glm::normalize(pivotWS - ci.cameraPosition);

  if (ncpplib::math::isZero(glm::length(axisWS))) {
    return ci;
  }

  glm::mat3 cameraPosRot = glm::mat3(glm::rotate(glm::mat4(1.0f), angle, axisWS));
  glm::vec3 newCameraPosition = pivotWS + cameraPosRot * (ci.cameraPosition - pivotWS);

  glm::vec3 cameraDirAxis = glm::cross(d, p);
  float cameraDirAngle = std::acos(clamp(glm::dot(d, p), -1.0f, 1.0f));

  if (ncpplib::math::isZero(glm::length(cameraDirAxis))) {
    cameraDirAxis = axisWS;
  }

  glm::mat3 cameraDirRot = glm::mat3(glm::rotate(glm::mat4(1.0f), -cameraDirAngle, cameraDirAxis));
  glm::vec3 newlookAtPosition = newCameraPosition + cameraDirRot * (pivotWS - newCameraPosition);

  CameraInfo res(ci);
  res.cameraPosition = newCameraPosition;
  res.lookAtPosition = newlookAtPosition;
  return res;
}

CameraInfo
moveCameraAroundPivot(
  const CameraInfo& ci, const glm::vec3& pivotWS,
  float dxPS, float dyPS, float factor) {

  float angle = sqrtf(dxPS * dxPS + dyPS * dyPS) * factor;
  glm::vec3 axis = getRotationAxisWS(ci, dxPS, dyPS);

  return moveCameraAroundPivot(ci, pivotWS, angle, axis);
}

CameraInfo
moveCameraToFrontOfPointer(const CameraInfo& ci, const glm::vec3& pos, const glm::vec3& normal, float distance) {
  CameraInfo res(ci);
  res.lookAtPosition = pos;
  res.cameraPosition = pos + normal * distance;
  return res;
}

glm::vec3
mapPos(const glm::mat4& m, const glm::vec3& vec) {
  glm::vec4 tmp = m * glm::vec4(vec, 1.0f);
  tmp /= tmp.w;
  return glm::vec3(tmp);
}

glm::vec3
mapDir(const glm::mat4& m, const glm::vec3& vec) {
  return glm::vec3(m * glm::vec4(vec, 0.0f));
}


std::vector<Face>
dividePolygonNaive(const Polygon& poly, const std::vector<glm::vec3>& vert) {
  // Check for concave polygons
  glm::vec3 e0 = vert[poly.vIndices[1]] - vert[poly.vIndices[0]];
  glm::vec3 e1 = vert[poly.vIndices[2]] - vert[poly.vIndices[1]];
  glm::vec3 normal = glm::cross(e0, e1);

  for (unsigned i = 1; i < poly.vIndices.size(); i++) {
    unsigned i1 = (i + 1) % poly.vIndices.size();
    unsigned i2 = (i + 2) % poly.vIndices.size();
    glm::vec3 e = vert[poly.vIndices[i1]] - vert[poly.vIndices[i]];
    glm::vec3 en = vert[poly.vIndices[i2]] - vert[poly.vIndices[i1]];
    if (glm::dot(glm::cross(e, en), normal) < 0) { // If concave
      throw std::runtime_error("Concave polygon");
    }
  }

  std::vector<Face> res;
  for (int i = 1; i + 1 < poly.numVertices(); i++) {
    Face f;
    f.vertexIndices[0] = poly.vIndices[0];
    f.vertexIndices[1] = poly.vIndices[i];
    f.vertexIndices[2] = poly.vIndices[i + 1];
    res.push_back(f);
  }

  return res;
}

std::vector<Face>
dividePolygonArrayNaive(const std::vector<Polygon>& polys, const std::vector<glm::vec3>& vert) {
  std::vector<Face> res;
  for (auto i = polys.begin(); i != polys.end(); i++) {
    const Polygon& p = *i;
    std::vector<Face> r = dividePolygonNaive(p, vert);
    res.insert(res.end(), r.begin(), r.end());
  }

  return res;
}

std::vector<Face>
dividePolygonRegular(const Polygon& poly_, std::vector<glm::vec3>& vert, std::vector<glm::vec3>& norm, float maxDistance, float minDistance) {
  std::vector<Face> naiveResult = dividePolygonNaive(poly_, vert);

  // Create the basis vertices (u_, v_) of the plain on which poly exists
  // The origin is v0
  glm::vec3 v0 = vert[poly_.vIndices[0]];
  glm::vec3 v1 = vert[poly_.vIndices[1]];
  glm::vec3 v2 = vert[poly_.vIndices[2]];
  glm::vec3 u_ = glm::normalize(v1 - v0);
  glm::vec3 e1 = v2 - v1;
  glm::vec3 w_ = glm::normalize(glm::cross(u_, e1));
  glm::vec3 v_ = glm::cross(w_, u_);

  // Calculate the bounding rectangle
  float uMax, uMin, vMax, vMin;
  uMax = vMax = -FLT_MAX;
  uMin = vMin = FLT_MAX;
  for (auto i = poly_.vIndices.begin(); i != poly_.vIndices.end(); i++) {
    float u = glm::dot(vert[*i] - v0, u_);
    float v = glm::dot(vert[*i] - v0, v_);
    uMax = std::max(uMax, u);
    vMax = std::max(vMax, v);
    uMin = std::min(uMin, u);
    vMin = std::min(vMin, v);
  }

  // Divide the edges of poly_
  Polygon poly;
  for (int i = 0; i < poly_.numVertices(); i++) {
    int ni = (i + 1) % poly_.numVertices();
    glm::vec3 vi = vert[poly_.vIndices[i]];
    glm::vec3 vni = vert[poly_.vIndices[ni]];
    int nDiv = (int)ceilf(glm::length(vni - vi) / maxDistance);

    poly.add(poly_.vIndices[i]);
    for (int j = 1; j < nDiv; j++) {
      glm::vec3 newV = vi + (vni - vi) * (float)j / (float)nDiv;
      int newI = vert.size();

      vert.push_back(newV);
      norm.push_back(w_);
      poly.add(newI);
    }
  }

  // Generate lattice points
  float uUnit = maxDistance;
  float vUnit = maxDistance * sinf(pi / 3);
  int numU = (int)ceilf((uMax - uMin) / uUnit);
  int numV = (int)ceilf((vMax - vMin) / vUnit);
  std::vector<std::vector<int> > latticeIndex(numV, std::vector<int>(numU, -1));
  std::vector<int> addedIndex;
  for (int i = 0; i < numV; i++) {
    for (int j = 0; j < numU; j++) {
      float u = uMin + uUnit * (j + (i % 2 == 1 ? 0.5f : 0));
      float v = vMin + vUnit * i;
      glm::vec3 p = v0 + u * u_ + v * v_;

      bool ok = true;
      // Judge whether p is too near other any other point
      for (int k = 0; k < poly.numVertices(); k++) {
        int nk = (k + 1) % poly.numVertices();

        glm::vec3 vk = vert[poly.vIndices[k]];
        glm::vec3 vnk = vert[poly.vIndices[nk]];

        if (glm::length(glm::cross(glm::normalize(vnk - vk), p - vk)) < minDistance) {
          ok = false;
          break;
        }
      }

      if (!ok) {
        continue;
      }

      // Judge whether p is inside the polygon
      for (auto k = naiveResult.begin(); k != naiveResult.end(); k++) {
        glm::vec3 fv0 = vert[k->vertexIndices[0]];
        glm::vec3 fv1 = vert[k->vertexIndices[1]];
        glm::vec3 fv2 = vert[k->vertexIndices[2]];
        float x0 = glm::dot(glm::cross(p - fv0, fv1 - fv0), w_);
        float x1 = glm::dot(glm::cross(p - fv1, fv2 - fv1), w_);
        float x2 = glm::dot(glm::cross(p - fv2, fv0 - fv2), w_);

        if ((x0 <= 0 && x1 <= 0 && x2 <= 0) || (x0 >= 0 && x1 >= 0 && x2 >= 0)) {
          // Use the lartice point
          latticeIndex[i][j] = vert.size();
          addedIndex.push_back(vert.size());
          vert.push_back(p);
          norm.push_back(w_);
        }
      }
    }
  }

  if (addedIndex.size() == 0) {
    return naiveResult;
  }

  std::vector<Face> res;
  std::vector<int> connectedIndices;
  for (int i = 0; i < poly.numVertices(); i++) {
    int ni = (i + 1) % poly.numVertices();

    float minLength = FLT_MAX;
    float minIndex = -1;
    for (auto j = addedIndex.begin(); j != addedIndex.end(); j++) {
      float l1 = glm::length(vert[poly.vIndices[i]] - vert[*j]);
      float l2 = glm::length(vert[poly.vIndices[ni]] - vert[*j]);
      float l = l1 + l2;
      if (l < minLength) {
        minLength = l;
        minIndex = *j;
      }
    }

    res.push_back(Face(poly.vIndices[i], poly.vIndices[ni], minIndex));
    connectedIndices.push_back(minIndex);
  }

  for (int i = 0; i < poly.numVertices(); i++) {
    int ni = (i + 1) % poly.numVertices();

    if (connectedIndices[i] != connectedIndices[ni]) {
      res.push_back(Face(connectedIndices[ni], connectedIndices[i], poly.vIndices[ni]));
    }
  }
  // Up pointing triangles
  for (int i = 0; i < numV - 1; i++) {
    for (int j = 0; j < numU - 1; j++) {
      int l0, l1, l2;
      if (i % 2 == 0) {
        l0 = latticeIndex[i][j];
        l1 = latticeIndex[i][j + 1];
        l2 = latticeIndex[i + 1][j];
      } else {
        l0 = latticeIndex[i][j];
        l1 = latticeIndex[i][j + 1];
        l2 = latticeIndex[i + 1][j + 1];
      }
      if (l0 >= 0 && l1 >= 0 && l2 >= 0) {
        res.push_back(Face(l0, l1, l2));
      }
    }
  }

  // Down pointing triangle
  for (int i = 1; i < numV; i++) {
    for (int j = 0; j < numU - 1; j++) {
      int l0, l1, l2;
      if (i % 2 == 0) {
        l0 = latticeIndex[i][j + 1];
        l1 = latticeIndex[i][j];
        l2 = latticeIndex[i - 1][j];
      } else {
        l0 = latticeIndex[i][j + 1];
        l1 = latticeIndex[i][j];
        l2 = latticeIndex[i - 1][j + 1];
      }
      if (l0 >= 0 && l1 >= 0 && l2 >= 0) {
        res.push_back(Face(l0, l1, l2));
      }
    }
  }
  return res;
}

bool
intersectPlane3(const glm::vec3& orig,  const glm::vec3& dir, const glm::vec3& v0, const glm::vec3& v1, const glm::vec3& v2, glm::vec3& pos) {
  glm::vec3 normal = glm::cross(v1 - v0, v2 - v0);
  if (ncpplib::math::isZero(glm::length(normal))) {
    return false;
  }

  normal = glm::normalize(normal);
  float dn = glm::dot(dir, normal);
  float von = glm::dot(orig - v0, normal);
  if (ncpplib::math::isZero(dn)) {
    return false;
  }

  float t = von / dn * -1.0f;
  pos = orig + dir * t;
  return true;
}

bool
intersect(const glm::vec3& orig, const glm::vec3& dir, const glm::vec3& v0, const glm::vec3& v1, const glm::vec3& v2, glm::vec3& pos) {
  glm::vec3 p;
  if (intersectPlane3(orig, dir, v0, v1, v2, p)) {
    glm::vec3 normal = glm::cross(v1 - v0, v2 - v0);

    float x0 = glm::dot(glm::cross(p - v0, v1 - v0), normal);
    float x1 = glm::dot(glm::cross(p - v1, v2 - v1), normal);
    float x2 = glm::dot(glm::cross(p - v2, v0 - v2), normal);

    if ((x0 <= 0 && x1 <= 0 && x2 <= 0) || (x0 >= 0 && x1 >= 0 && x2 >= 0)) {
      pos = p;
      return true;
    }
  }

  return false;
}

}
