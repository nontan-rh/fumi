// -*- C++ -*-

#ifndef FUMI_MODIFICATION_GUARD_H_
#define FUMI_MODIFICATION_GUARD_H_

namespace fumi {

template <class T>
class ModificationGuard {
  ModificationGuard();
  ModificationGuard(const ModificationGuard& mg);

public:
  ModificationGuard(T& t_) : t(&t_) {
    t->beginModification();
  }

  ~ModificationGuard() {
    t->endModification();
  }

private:
  T* t;
};

}

#endif // FUMI_MODIFICATION_GUARD_H_
