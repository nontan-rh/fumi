// -*- C++ -*-

#ifndef FUMI_BRUSH_H_
#define FUMI_BRUSH_H_

#include <glm/glm.hpp>

namespace fumi {

class IBrush {
public:
  virtual glm::vec3 operator()(const glm::vec3& vertexPos, const glm::vec3& vertexNormal) = 0;
};

class MonomialBrush : public IBrush {
  MonomialBrush();
public:
  MonomialBrush(const glm::vec3& pointerPos, const glm::vec3& pointerNormal, float intensity, float order, float radius);

  glm::vec3 pointerPos;
  glm::vec3 pointerNormal;
  float intensity;
  float order;
  float radius;

  virtual glm::vec3 operator()(const glm::vec3& vertexPos, const glm::vec3& vertexNormal);
};

}

#endif // FUMI_BRUSH_H_
