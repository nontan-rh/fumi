// -*- C++ -*-
#include <string>
#include <vector>
#include <sstream>
#include <memory>

#include "common.h"

namespace fumi {

std::vector<std::string>
split(const std::string &str, char delim){
  std::istringstream ss(str);
  std::string tmp;
  std::vector<std::string> res;
  while(getline(ss, tmp, delim)) {
    res.push_back(tmp);
  }
  return res;
}

}
