#include <vector>
#include <set>
#include <map>
#include <stack>

#include <glm/glm.hpp>
#include <ncpplib/math/ops.h>
#include <ncpplib/math/modulo.h>
#include <ncpplib/algorithm.h>

#include "common.h"
#include "geometry.h"
#include "sculptable_mesh.h"
#include "userconfig.h"

#include "mesh_manipulation.h"

namespace fumi {

static glm::vec3
intersectionYZPlane(const glm::vec3& p, const glm::vec3& q) {
  glm::vec3 v = p + (q - p) * (-p.x / (q.x - p.x));
  v.x = 0;
  return v;
}

static glm::vec3
projYZPlane(const glm::vec3& p) {
  glm::vec3 v = p;
  v.x = 0;
  return v;
}

static glm::vec3
mirrorYZPlane(const glm::vec3& p) {
  glm::vec3 v = p;
  v.x = -v.x;
  return v;
}

// sign : true -> +, false -> -
static bool
isSignSide(const glm::vec3& p, bool sign) {
  return sign ? p.x >= 0 : p.x <= 0;
}

static bool
faceSign(const glm::vec3& v0, const glm::vec3& v1, const glm::vec3& v2) {
  return v0.x > 0 || v1.x > 0 || v2.x > 0;
}

int
intersection(const SculptableMesh& mesh, const glm::vec3& orig, const glm::vec3& dir, glm::vec3& res, glm::vec3& normal) {
  for (int i = 0; i < mesh.faces.size(); i++) {
    if (!mesh.faceAllocator->isUsed(i)) {
      continue;
    }

    const Face& f = mesh.faces[i];
    const glm::vec3& v0 = mesh.vertices[f.vertexIndices[0]];
    const glm::vec3& v1 = mesh.vertices[f.vertexIndices[1]];
    const glm::vec3& v2 = mesh.vertices[f.vertexIndices[2]];
    glm::vec3 n = glm::cross(v1 - v0, v2 - v0);
    glm::vec3 pos;
    if (glm::dot(n, dir) < 0 && intersect(orig, dir, v0, v1, v2, pos)) {
      res = pos;
      normal = glm::normalize(n);
      return i;
    }
  }
  return -1;
}

std::vector<int>
chooseVertices(SculptableMesh& mesh, const glm::vec3& orig, const glm::vec3& dir, float radius) {
  std::set<int> candidateIndices;
  std::vector<std::pair<float, int> > candidateDistances(0);

  bool origXSign = orig.x > 0;

  glm::vec3 d = glm::normalize(dir);
  glm::vec3 o = orig - d * UserConfig::algoPointerBackOnSelectionFactor;

  if (ncpplib::math::isZero(glm::length(d))) {
    return std::vector<int>();
  }
  for (int i = 0; i < mesh.vertices.size(); i++) {
    if (!mesh.vertexAllocator->isUsed(i)) {
      continue;
    }

    glm::vec3 o2v = mesh.vertices[i] - o;
    if (glm::length(glm::cross(o2v, d)) < radius) {
      float dotProd = glm::dot(o2v, d);

      const glm::vec3& v = mesh.vertices[i];
      if (dotProd >= 0 && glm::dot(mesh.normals[i], d) <= 0
        && (!mesh.symmetrical || v.x > 0 == origXSign || v.x == 0)) {

        candidateIndices.insert(i);
        candidateDistances.push_back(std::make_pair(dotProd, i));
      }
    }
  }

  // candidateIndices is already sorted in ascending order
  std::vector<int> res;

  if (!candidateDistances.empty()) {
    int nearestVertex = std::min_element(candidateDistances.begin(), candidateDistances.end())->second;

    std::set<int> traversed;
    std::stack<int> dfsStack;

    dfsStack.push(nearestVertex);
    traversed.insert(nearestVertex);

    while (!dfsStack.empty()) {
      int vertex = dfsStack.top();
      dfsStack.pop();
      res.push_back(vertex);

      for (unsigned i = 0; i < mesh.containedFaces[vertex].size(); i++) {
        for (int j = 0; j < 3; j++) {
          int nextVertex = mesh.faces[mesh.containedFaces[vertex][i]].vertexIndices[j];
          if (candidateIndices.find(nextVertex) != candidateIndices.end()
            && traversed.find(nextVertex) == traversed.end()) {

            traversed.insert(nextVertex);
            dfsStack.push(nextVertex);
          }
        }
      }
    }
  }

  return res;
}

std::vector<int>
chooseFace(SculptableMesh& mesh, const glm::vec3& orig, const glm::vec3& dir, float radius) {
  std::vector<int> selectedVertices = chooseVertices(mesh, orig, dir, radius);

  if (selectedVertices.size() == 0) {
    glm::vec3 r, n;
    int faceIndex = intersection(mesh, orig, dir, r, n);
    if (faceIndex >= 0) {
      return std::vector<int>(1, faceIndex);
    } else {
      return std::vector<int>(0);
    }
  }

  std::vector<int> res;
  for (auto i = selectedVertices.begin(); i != selectedVertices.end(); i++) {
    const std::vector<int>& faces = mesh.containedFaces[*i];
    res.insert(res.end(), faces.begin(), faces.end());
  }
  std::sort(res.begin(), res.end());
  res.erase(std::unique(res.begin(), res.end()), res.end());
  return res;
}

void
recalcNormal(SculptableMesh& mesh, int index) {
  const std::vector<int>& cf = mesh.containedFaces[index];
  glm::vec3 v(0.0f, 0.0f, 0.0f);
  for (auto i = cf.begin(); i != cf.end(); i++) {
    const Face& f = mesh.faces[*i];
    glm::vec3 v0 = mesh.vertices[f.vertexIndices[0]];
    glm::vec3 v1 = mesh.vertices[f.vertexIndices[1]];
    glm::vec3 v2 = mesh.vertices[f.vertexIndices[2]];
    v += glm::normalize(glm::cross(v1 - v0, v2 - v1));
  }
  mesh.normals.set(index, glm::normalize(v));
}

void
sculpt(SculptableMesh& mesh, IBrush& brush, const glm::vec3& orig, const glm::vec3& dir, float radius) {
  ModificationGuard<BufferedVector<glm::vec3> > mgVertices(mesh.vertices);
  ModificationGuard<BufferedVector<glm::vec3> > mgNormals(mesh.normals);
  ModificationGuard<BufferedVector<glm::vec3> > mgColors(mesh.colors);
  ModificationGuard<BufferedVector<Face> > mgFaces(mesh.faces);

  std::vector<int> selected = chooseVertices(mesh, orig, dir, radius);

  std::for_each(selected.begin(), selected.end(), [&mesh, &brush] (int i) {
    glm::vec3 delta = brush(mesh.vertices[i], mesh.normals[i]);
    if (mesh.symmetrical && mesh.symmetricalVertex[i] != i) {
      mesh.vertices.set(i, mesh.vertices[i] + delta);
      int j = mesh.symmetricalVertex[i];
      mesh.vertices.set(j, mesh.vertices[j] + mirrorYZPlane(delta));
    } else {
      delta = projYZPlane(delta);
      mesh.vertices.set(i, mesh.vertices[i] + delta);
    }
  });

  std::for_each(selected.begin(), selected.end(), [&mesh] (int i) {
    recalcNormal(mesh, i);
    if (mesh.symmetrical) {
      int j = mesh.symmetricalVertex[i];
      if (i != j) {
        recalcNormal(mesh, j);
      }
    }
  });
}

// This function assumes that one edge is shared by exactly two faces
int
getEdgeSharingFace(const SculptableMesh& mesh, int v0, int v1, int exclude) {
  const std::vector<int>& cf0 = mesh.containedFaces[v0];
  const std::vector<int>& cf1 = mesh.containedFaces[v1];
  for (auto i = cf0.begin(); i != cf0.end(); i++) {
    if (*i == exclude) {
      continue;
    }

    for (auto j = cf1.begin(); j != cf1.end(); j++) {
      if (*i == *j) {
        return *i;
      }
    }
  }
  throw std::runtime_error("Invalid mesh");
  return -1;
}

int
getFaceByVertices(SculptableMesh& mesh, int v0, int v1, int v2) {
  std::vector<int>& cf0 = mesh.containedFaces[v0];
  std::vector<int>& cf1 = mesh.containedFaces[v1];
  std::vector<int>& cf2 = mesh.containedFaces[v2];
  std::sort(cf0.begin(), cf0.end());
  std::sort(cf1.begin(), cf1.end());
  std::sort(cf2.begin(), cf2.end());

  std::vector<int> i1(cf0.size() + cf1.size());
  auto i1End = std::set_intersection(cf0.begin(), cf0.end(), cf1.begin(), cf1.end(), i1.begin());


  std::vector<int> i2(std::distance(i1.begin(), i1End) + cf2.size());
  auto i2End = std::set_intersection(i1.begin(), i1End, cf2.begin(), cf2.end(), i2.begin());

  int numFaces = std::distance(i2.begin(), i2End);
  int res;
  if (numFaces == 0) {
    res = -1;
  } else if (numFaces == 1) {
    res = i2[0];
  } else {
    throw std::runtime_error("i");
  }
  return res;
}

void
calcSymmetricalFace(SculptableMesh& mesh, int fi) {
  const Face& f = mesh.faces[fi];
  int sv0 = mesh.symmetricalVertex[f.vertexIndices[0]];
  int sv1 = mesh.symmetricalVertex[f.vertexIndices[1]];
  int sv2 = mesh.symmetricalVertex[f.vertexIndices[2]];
  int si = getFaceByVertices(mesh, sv0, sv1, sv2);
  assert(si != -1);
  mesh.symmetricalFace[fi] = si;
  mesh.symmetricalFace[si] = fi;
}

void
printFace(Face f) {
  std::cout << f.vertexIndices[0] << "," << f.vertexIndices[1] << "," << f.vertexIndices[2] << std::endl;
}

struct DivisionInfo {
  int faceIndex;
  float maxLength;
  int divisionRest;

  DivisionInfo(int fi, float ml, int dr)
    : faceIndex(fi), maxLength(ml), divisionRest(dr) { }


  DivisionInfo()
    : faceIndex(0), maxLength(-1), divisionRest(0) { }
};

// f0i : The face to divide
// f0ei : The edge of f0i to divide
// nvi : The index of new vertex
// return : The face sharing f0ei

static int
divideFaceOneAux(SculptableMesh& mesh, int f0i, int f0ei, int nvi, std::vector<int>& divided_, std::vector<int>& accompanied_) {
  int v0i = mesh.faces[f0i].vertexIndices[f0ei];
  int v1i = mesh.faces[f0i].vertexIndices[ncpplib::math::modulo::uIncr(f0ei, 3)];
  int f1i = getEdgeSharingFace(mesh, v0i, v1i, f0i);

  int nf0i = mesh.faceAllocator->alloc();
  int nf1i = mesh.faceAllocator->alloc();

  Face f0 = mesh.faces[f0i];
  int f0pvi = f0.vertexIndices[ncpplib::math::modulo::uDecr(f0ei, 3)];
  f0.vertexIndices[f0ei] = nvi;

  Face nf0 = mesh.faces[f0i];
  nf0.vertexIndices[ncpplib::math::modulo::uIncr(f0ei, 3)] = nvi;

  mesh.faces.set(f0i, f0);
  mesh.faces.set(nf0i, nf0);

  Face f1 = mesh.faces[f1i];
  int ei = ncpplib::algorithm::findIfIndex(0, 3, [&f1, &v0i] (int i) { return f1.vertexIndices[i] == v0i; });
  int f1pvi = f1.vertexIndices[ncpplib::math::modulo::uIncr(ei, 3)];
  f1.vertexIndices[ncpplib::math::modulo::uDecr(ei, 3)] = nvi;

  Face nf1 = mesh.faces[f1i];
  nf1.vertexIndices[ei] = nvi;

  mesh.faces.set(f1i, f1);
  mesh.faces.set(nf1i, nf1);

  // Update mesh.containedFaces
  std::vector<std::vector<int> >& cf = mesh.containedFaces;
  cf[nvi].push_back(f0i);
  cf[nvi].push_back(nf0i);
  cf[nvi].push_back(f1i);
  cf[nvi].push_back(nf1i);

  cf[v0i].erase(std::remove(cf[v0i].begin(), cf[v0i].end(), f0i));
  cf[v0i].push_back(nf0i);

  cf[v1i].erase(std::remove(cf[v1i].begin(), cf[v1i].end(), f1i));
  cf[v1i].push_back(nf1i);

  cf[f0pvi].push_back(nf0i);
  cf[f1pvi].push_back(nf1i);

  divided_.push_back(f0i);
  divided_.push_back(nf0i);
  accompanied_.push_back(f1i);
  accompanied_.push_back(nf1i);

  return f1i;
}

static bool
divideFaceOne(SculptableMesh& mesh, DivisionInfo& face, int& f1i_, std::vector<int>& divided_, std::vector<int>& accompanied_) {
  using ncpplib::math::isZero;

  bool update = false;
  int f0i = face.faceIndex;
  const Face& f = mesh.faces[f0i];

  std::pair<float, int> edgeLength[3];
  for (int j = 0; j < 3; j++) {
    edgeLength[j] = std::make_pair(
      glm::length(mesh.vertices[f.vertexIndices[j]] - mesh.vertices[f.vertexIndices[(j + 1) % 3]]),
      j
    );
  }
  std::sort(edgeLength, edgeLength + 3);

  std::pair<float, int> longestEdge = edgeLength[2];
  if (longestEdge.first <= face.maxLength) {
    return false;
  }

  int f0ei = longestEdge.second;

  // Divide one to two triangles
  int v0i = f.vertexIndices[f0ei];
  int v1i = f.vertexIndices[ncpplib::math::modulo::uIncr(f0ei, 3)];

  bool crossingCenterLine = false;
  if (isZero(mesh.vertices[v0i].x) && isZero(mesh.vertices[v1i].x)) {
    crossingCenterLine = true;
  }

  glm::vec3 pos = (mesh.vertices[v0i] + mesh.vertices[v1i]) * 0.5f;
  glm::vec3 normal = glm::normalize(mesh.normals[v0i] + mesh.normals[v1i]);
  glm::vec3 color = (mesh.colors[v0i] + mesh.colors[v1i]) * 0.5f;

  // Create a new vertex
  int nvi = mesh.vertexAllocator->alloc();
  int snvi;
  mesh.vertices.set(nvi, pos);
  mesh.normals.set(nvi, normal);
  mesh.colors.set(nvi, color);
  if (mesh.symmetrical && !crossingCenterLine) {
    snvi = mesh.vertexAllocator->alloc();
    mesh.vertices.set(snvi, mirrorYZPlane(pos));
    mesh.normals.set(snvi, mirrorYZPlane(normal));
    mesh.colors.set(snvi, color);
    mesh.symmetricalVertex[nvi] = snvi;
    mesh.symmetricalVertex[snvi] = nvi;
  } else if (mesh.symmetrical && crossingCenterLine) {
    mesh.symmetricalVertex[nvi] = nvi;
  }

  // Rearrange faces
  std::vector<int> divided(0);
  std::vector<int> accompanied(0);
  int vi = f.vertexIndices[f0ei];
  int f1i = divideFaceOneAux(mesh, f0i, f0ei, nvi, divided, accompanied);
  int sf1i;
  if (mesh.symmetrical && !crossingCenterLine) {
    int sf0i = mesh.symmetricalFace[f0i];
    const Face& sf0 = mesh.faces[sf0i];
    int svi = mesh.symmetricalVertex[vi];

    if (f1i != sf0i) {
      int sf0ei = ncpplib::algorithm::findIfIndex(0, 3, [&sf0, &svi] (int i) { return svi == sf0.vertexIndices[i]; });
      assert(sf0ei != 3);
      std::vector<int> sDivided(0);
      std::vector<int> sAccompanied(0);
      sf0ei = ncpplib::math::modulo::uDecr(sf0ei, 3);
      sf1i = divideFaceOneAux(mesh, sf0i, sf0ei, snvi, sDivided, sAccompanied);
      for (auto i : sDivided) {
        calcSymmetricalFace(mesh, i);
      }
      for (auto i : sAccompanied) {
        calcSymmetricalFace(mesh, i);
      }
    }
  } else if (mesh.symmetrical && crossingCenterLine) {
    for (auto i : divided) {
      calcSymmetricalFace(mesh, i);
    }
    for (auto i : accompanied) {
      calcSymmetricalFace(mesh, i);
    }
  }

  f1i_ = f1i;
  divided_ = std::move(divided);
  accompanied_ = std::move(accompanied);
  return true;
}

bool
divideFaces(SculptableMesh& mesh, const std::vector<DivisionInfo>& faces_, float maxLengthIncreaseFactor) {
  bool update = false;

  std::vector<DivisionInfo> faces = faces_;
  std::vector<DivisionInfo> nextFaces;

  int cnt = 0;
  while (!faces.empty()) {
    nextFaces.clear();

    for (auto i = faces.begin(); i != faces.end(); i++) {
      if (i->maxLength < 0 || i->divisionRest <= 0) {
        continue;
      }

      std::vector<int> divided;
      std::vector<int> accompanied;
      int f1i;
      update = divideFaceOne(mesh, *i, f1i, divided, accompanied) || update;

      float f1MaxLength = i->maxLength;
      // Remove the divided face from the list
      auto f1Iter = std::find_if(faces.begin(), faces.end(), [f1i] (const DivisionInfo& f) { return f.faceIndex == f1i; });
      if (f1Iter != faces.end()) {
        f1Iter->maxLength = -1.0f; // The triangle is already divided
      } else {
        f1MaxLength = i->maxLength * maxLengthIncreaseFactor;
      }

      // Add the newly created faces to the next list
      if (i->divisionRest > 1) {
        for (auto j : divided) {
          nextFaces.push_back(DivisionInfo(j, i->maxLength, i->divisionRest - 1));
        }
        for (auto j : accompanied) {
          nextFaces.push_back(DivisionInfo(j, f1MaxLength, i->divisionRest - 1));
        }
      }
    }
    faces = nextFaces;
  }
  return update;
}

void
divide(SculptableMesh& mesh, const glm::vec3& orig, const glm::vec3& dir, float radius, float maxLength, float maxLengthIncreaseFactor) {
  ModificationGuard<BufferedVector<glm::vec3> > mgVertices(mesh.vertices);
  ModificationGuard<BufferedVector<glm::vec3> > mgNormals(mesh.normals);
  ModificationGuard<BufferedVector<glm::vec3> > mgColors(mesh.colors);
  ModificationGuard<BufferedVector<Face> > mgFaces(mesh.faces);

  //sanityCheck(mesh);

  bool update = false;
  do {
    std::vector<int> selectedFaces = chooseFace(mesh, orig, dir, radius);

    std::vector<DivisionInfo> v(selectedFaces.size());
    for (unsigned i = 0; i < v.size(); i++) {
      v[i] = DivisionInfo(selectedFaces[i], maxLength, 3);
    }
    update = divideFaces(mesh, v, maxLengthIncreaseFactor);
  } while (update);
}

static int
getDividedVertex(SculptableMesh& mesh, std::map<Edge, int>& created, int v0, int v1) {
  auto e = Edge(v0, v1);
  auto i = created.find(e);
  int res = -1;
  if (i == created.end()) {
    res = mesh.vertexAllocator->alloc();
    created[e] = res;
  } else {
    res = i->second;
  }

  return res;
}

// Make the shape symmetrical using the vertices and the faces whose x is greater than 0
void
symmetrize(SculptableMesh& mesh) {
  using ncpplib::algorithm::findIfIndex;
  using ncpplib::math::isZero;
  using ncpplib::math::isPlus;
  using ncpplib::math::isMinus;

  ModificationGuard<BufferedVector<glm::vec3> > mgVertices(mesh.vertices);
  ModificationGuard<BufferedVector<glm::vec3> > mgNormals(mesh.normals);
  ModificationGuard<BufferedVector<glm::vec3> > mgColors(mesh.colors);
  ModificationGuard<BufferedVector<Face> > mgFaces(mesh.faces);

  mesh.setSymmetrical(true);

  std::map<Edge, int> newVertices;

  // Cut faces with yz-plane
  for (int i = 0; i < mesh.faces.size(); i++) {
    if (!mesh.faceAllocator->isUsed(i)) {
      continue;
    }

    glm::vec3 vs[3];
    const GLuint (&vis)[3] = mesh.faces[i].vertexIndices;
    for (int j = 0; j < 3; j++) {
      vs[j] = mesh.vertices[vis[j]];
    }

    int numZeros = std::count_if(vs, vs + 3, [] (const glm::vec3& v) { return isZero(v.x); });
    int numPluses = std::count_if(vs, vs + 3, [] (const glm::vec3& v) { return isPlus(v.x); });

    if (numZeros == 0) {
      // No vertex is on the plane
      if (numPluses == 1 || numPluses == 2) {
        int pvi;
        if (numPluses == 1) {
          pvi = findIfIndex(0, 3, [&vs] (int j) { return isPlus(vs[j].x); });
        } else { // numPluses == 2
          pvi = findIfIndex(0, 3, [&vs] (int j) { return isMinus(vs[j].x); });
        }
        int npvi = ncpplib::math::modulo::uIncr(pvi, 3);
        int ppvi = ncpplib::math::modulo::uDecr(pvi, 3);

        int newnvi = getDividedVertex(mesh, newVertices, vis[pvi], vis[npvi]);
        int newpvi = getDividedVertex(mesh, newVertices, vis[ppvi], vis[pvi]);
        int newcvi = vis[pvi];

        glm::vec3 newnv = intersectionYZPlane(vs[pvi], vs[npvi]);
        glm::vec3 newpv = intersectionYZPlane(vs[ppvi], vs[pvi]);

        mesh.vertices.set(newnvi, newnv);
        mesh.vertices.set(newpvi, newpv);

        mesh.colors.set(newnvi, (mesh.colors[vis[pvi]] + mesh.colors[vis[npvi]]) / 2.0f);
        mesh.colors.set(newpvi, (mesh.colors[vis[pvi]] + mesh.colors[vis[ppvi]]) / 2.0f);

        if (numPluses == 1) {
          mesh.faces.set(i, Face(newpvi, newcvi, newnvi));
        } else { // numPluses == 2
          int newfi = mesh.faceAllocator->alloc();
          mesh.faces.set(newfi, Face(newnvi, vis[npvi], newpvi));
          mesh.faces.set(i, Face(vis[npvi], vis[ppvi], newpvi));
        }
      } else { // numPluses == 0 || numPluses == 3
        // Do nothing
      }
    } else if (numZeros == 1) {
      // One vertex is on the plane
      int pvi = findIfIndex(0, 3, [&vs] (int j) { return isZero(vs[j].x); });
      if (numPluses == 1) {
        int npvi = ncpplib::math::modulo::uIncr(pvi, 3);
        int ppvi = ncpplib::math::modulo::uDecr(pvi, 3);

        int newvi = getDividedVertex(mesh, newVertices, vis[npvi], vis[ppvi]);

        glm::vec3 newv = intersectionYZPlane(vs[npvi], vs[ppvi]);
        mesh.vertices.set(newvi, newv);

        mesh.colors.set(newvi, (mesh.colors[vis[npvi]] + mesh.colors[vis[ppvi]]) / 2.0f);

        if (isPlus(vs[npvi].x)) {
          mesh.faces.set(i, Face(vis[pvi], vis[npvi], newvi));
        } else { // isPlus(vs[ppvi].x)
          mesh.faces.set(i, Face(vis[pvi], newvi, vis[ppvi]));
        }
      } else { // numPluses == 0 || numPluses == 2
        // Do nothing
      }
      mesh.vertices.set(vis[pvi], projYZPlane(mesh.vertices[vis[pvi]]));
    } else if (numZeros == 2) {
      // One edge is on the plane
      for (auto j : vis) {
        if (isZero(mesh.vertices[j].x)) {
          mesh.vertices.set(j, projYZPlane(mesh.vertices[j]));
        }
      }
    } else if (numZeros == 3) {
      // The triangle is on the plane
      for (auto j : vis) {
        mesh.vertices.set(j, projYZPlane(mesh.vertices[j]));
      }
    }
  }

  // Remove faces which are in x < 0
  for (int i = 0; i < mesh.faces.size(); i++) {
    if (!mesh.faceAllocator->isUsed(i)) {
      continue;
    }
    glm::vec3 vs[3];
    const GLuint (&vis)[3] = mesh.faces[i].vertexIndices;
    for (int j = 0; j < 3; j++) {
      vs[j] = mesh.vertices[vis[j]];
    }

    if (std::any_of(vis, vis + 3, [&mesh] (int j) { return isMinus(mesh.vertices[j].x); })) {
      mesh.faces.set(i, Face(0, 0, 0));
      mesh.faceAllocator->free(i);
    }
  }

  // Remove vertices which are in x < 0
  for (int i = 0; i < mesh.faces.size(); i++) {
    if (!mesh.vertexAllocator->isUsed(i)) {
      continue;
    }

    if (isMinus(mesh.vertices[i].x)) {
      mesh.vertices.set(i, glm::vec3(0, 0, 0));
      mesh.vertexAllocator->free(i);
    }
  }

  // Make symmetrical vertices
  std::set<int> newlyCreatedVertices;
  for (int i = 0; i < mesh.vertices.size(); i++) {
    if (!mesh.vertexAllocator->isUsed(i)
      || newlyCreatedVertices.find(i) != newlyCreatedVertices.end()) {

      continue;
    }

    const glm::vec3& v = mesh.vertices[i];
    if (isZero(v.x)) {
      mesh.symmetricalVertex[i] = i;
    } else {
      int nvi = mesh.vertexAllocator->alloc();
      mesh.vertices.set(nvi, glm::vec3(-v.x, v.y, v.z));
      mesh.colors.set(nvi, mesh.colors[i]);
      mesh.symmetricalVertex[i] = nvi;
      mesh.symmetricalVertex[nvi] = i;
      newlyCreatedVertices.insert(nvi);
    }
  }

  // Make symmetrical faces
  std::set<int> newlyCreatedFaces;
  for (int i = 0; i < mesh.faces.size(); i++) {
    if (!mesh.faceAllocator->isUsed(i)
      || newlyCreatedFaces.find(i) != newlyCreatedFaces.end()) {

      continue;
    }

    Face f;
    for (int j = 0; j < 3; j++) {
      f.vertexIndices[2 - j] = mesh.symmetricalVertex[mesh.faces[i].vertexIndices[j]];
    }

    int nfi = mesh.faceAllocator->alloc();
    mesh.faces.set(nfi, f);
    mesh.symmetricalFace[i] = nfi;
    mesh.symmetricalFace[nfi] = i;
    newlyCreatedFaces.insert(nfi);
  }

  mesh.calculateContainedFaces();
  for (int i = 0; i < mesh.vertices.size(); i++) {
    if (!mesh.vertexAllocator->isUsed(i)) {
      continue;
    }

    recalcNormal(mesh, i);
  }
}

}
