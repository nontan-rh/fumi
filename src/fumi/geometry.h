// -*- C++ -*-

#ifndef FUMI_GEOMETRY_H_
#define FUMI_GEOMETRY_H_

#include <glm/glm.hpp>

#include "common.h"

namespace fumi {

struct CameraInfo {
  glm::vec3 cameraPosition;
  glm::vec3 lookAtPosition;
  glm::vec3 upDirection;
  float fovY;
  float zFar;
  float zNear;
  float aspectRatio;
  float viewportHeight;
  float viewportWidth;
};

glm::vec3 proj2WorldDirection(const CameraInfo& ci, float x, float y);
void getUnitVectorCSinWS(const CameraInfo& ci, glm::vec3& exWS, glm::vec3& eyWS, glm::vec3& ezWS);
glm::vec3 getRotationAxisWS(const CameraInfo& ci, float dxPS, float dyPS);
CameraInfo moveCameraAroundPivot(const CameraInfo& ci, const glm::vec3& pivotWS, float angle, const glm::vec3& axisWS);
CameraInfo moveCameraAroundPivot(const CameraInfo& ci, const glm::vec3& pivotWS, float dxPS, float dyPS, float factor);
CameraInfo moveCameraToFrontOfPointer(const CameraInfo& ci, const glm::vec3& pos, const glm::vec3& normal, float distance);
glm::mat4 getRotationMatrix(const glm::vec3& from, const glm::vec3& to);
glm::vec3 mapPos(const glm::mat4& m, const glm::vec3& vec);
glm::vec3 mapDir(const glm::mat4& m, const glm::vec3& vec);

std::vector<Face> dividePolygonNaive(const Polygon& poly, const std::vector<glm::vec3>& vert);
std::vector<Face> dividePolygonArrayNaive(const std::vector<Polygon>& poly, const std::vector<glm::vec3>& vert);
std::vector<Face> dividePolygonRegular(const Polygon& poly, std::vector<glm::vec3>& vert, std::vector<glm::vec3>& norm, float minDistance, float maxDistance);

bool intersectPlane3(const glm::vec3& orig,  const glm::vec3& dir, const glm::vec3& v0, const glm::vec3& v1, const glm::vec3& v2, glm::vec3& pos);
bool intersect(const glm::vec3& orig, const glm::vec3& dir, const glm::vec3& v0, const glm::vec3& v1, const glm::vec3& v2, glm::vec3& pos);

}

#endif // FUMI_GEOMETRY_H_
