// -*- C++ -*-

#include <cmath>

#include <iostream>
#include <memory>
#include <vector>
#include <algorithm>
#include <utility>
#include <map>

#include <glm/glm.hpp>

#include "common.h"
#include "geometry.h"

#include "primitive_loader.h"

namespace fumi {

std::vector<glm::vec3>
PrimitiveLoader::getVertices() {
  return vertices;
}

std::vector<Face>
PrimitiveLoader::getFaces() {
  return faces;
}

std::vector<glm::vec3>
PrimitiveLoader::getNormals() {
  return normals;
}

std::vector<glm::vec3>
PrimitiveLoader::getColors() {
  return std::vector<glm::vec3>(vertices.size(), glm::vec3(0.7f, 0.7f, 0.7f));
}

int
PrimitiveLoader::numFaces() {
  return faces.size();
}

int
PrimitiveLoader::numVertices() {
  return vertices.size();
}

TorusLoader::TorusLoader(float innerRadius_, float outerRadius_, int sides_, int rings_)
  : innerRadius(innerRadius_), outerRadius(outerRadius_),
    sides(sides_), rings(rings_) {
}

bool
TorusLoader::load() {
  if (innerRadius < 0 || outerRadius < innerRadius || sides < 3 || rings < 3) {
    return false;
  }

  for (int i = 0; i < rings; i++) {
    for (int j = 0; j < sides; j++) {
      Polygon p;
      int i_next = (i + 1) % rings;
      int j_next = (j + 1) % sides;
      p.add(i       * sides + j);
      p.add(i       * sides + j_next);
      p.add(i_next  * sides + j_next);
      p.add(i_next  * sides + j);
      polygons.push_back(p);

      float latitude = pi * 2 * i / rings;
      float longitude = pi * 2 * j / sides;
      vertices.push_back(glm::vec3(
        (outerRadius + innerRadius * cosf(latitude)) * cosf(longitude),
        (outerRadius + innerRadius * cosf(latitude)) * sinf(longitude),
        innerRadius * sinf(latitude)
      ));

      normals.push_back(glm::normalize(glm::vec3(
        cosf(longitude),
        sinf(longitude),
        sinf(latitude)
      )));
    }
  }

  faces = dividePolygonArrayNaive(polygons, vertices);

  return true;
}

SphereLoader::SphereLoader(float radius_, int division_)
  : radius(radius_), division(division_) {
}

static std::pair<int, int>
makeUnorderedPair(int i, int j) {
  return std::make_pair(std::min(i, j), std::max(i, j));
}

int
getDividedVertex(
  std::map<std::pair<int, int>, int>& m,
  std::vector<glm::vec3>& vertices,
  int v0, int v1, float radius) {

  std::pair<int, int> e = makeUnorderedPair(v0, v1);
  auto i = m.find(e);
  int res;
  if (i == m.end()) {
    // Create vertex
    res = vertices.size();
    glm::vec3 newVertexPos = glm::normalize(vertices[v0] + vertices[v1]) * radius;
    vertices.push_back(newVertexPos);
    m[e] = res;
  } else {
    res = i->second;
  }

  return res;
}

bool
SphereLoader::load() {
  // Generate an octahedron

  vertices.push_back(glm::vec3(0    , 0    , 1.0f ) * radius); // 0
  vertices.push_back(glm::vec3(1.0f , 0    , 0    ) * radius); // 1
  vertices.push_back(glm::vec3(0    , 1.0f , 0    ) * radius); // 2
  vertices.push_back(glm::vec3(-1.0f, 0    , 0    ) * radius); // 3
  vertices.push_back(glm::vec3(0    , -1.0f, 0    ) * radius); // 4
  vertices.push_back(glm::vec3(0    , 0    , -1.0f) * radius); // 5

  std::vector<Face> f[2];
  f[0].push_back(Face(0, 1, 2));
  f[0].push_back(Face(0, 2, 3));
  f[0].push_back(Face(0, 3, 4));
  f[0].push_back(Face(0, 4, 1));
  f[0].push_back(Face(5, 2, 1));
  f[0].push_back(Face(5, 3, 2));
  f[0].push_back(Face(5, 4, 3));
  f[0].push_back(Face(5, 1, 4));

  std::map<std::pair<int, int>, int> dividedVertex;

  for (int cnt = 0; cnt < division; cnt++) {
    int ncnt = (cnt + 1) & 1;
    int ccnt = cnt & 1;
    f[ncnt].clear();

    for (auto i = f[ccnt].begin(); i != f[ccnt].end(); i++) {
      int v0 = i->vertexIndices[0];
      int v1 = i->vertexIndices[1];
      int v2 = i->vertexIndices[2];
      int v01 = getDividedVertex(dividedVertex, vertices, v0, v1, radius);
      int v12 = getDividedVertex(dividedVertex, vertices, v1, v2, radius);
      int v20 = getDividedVertex(dividedVertex, vertices, v2, v0, radius);

      f[ncnt].push_back(Face(v0, v01, v20));
      f[ncnt].push_back(Face(v01, v1, v12));
      f[ncnt].push_back(Face(v20, v01, v12));
      f[ncnt].push_back(Face(v20, v12, v2));
    }
  }

  faces = f[division & 1];

  normals.resize(vertices.size());
  for (unsigned i = 0; i < vertices.size(); i++) {
    normals[i] = glm::normalize(vertices[i]);
  }

  return true;
}

CylinderLoader::CylinderLoader(float radius1_, float radius2_, float height_, int slices_, int stacks_)
  : radius1(radius1_), radius2(radius2_), height(height_), slices(slices_),
    stacks(stacks_) {
}

bool
CylinderLoader::load() {
  // vertices[stk * slices + slc]
  for (int stk = 0; stk <= stacks; stk++) {
    for (int slc = 0; slc < slices; slc++) {
      float radius = radius1 + (radius2 - radius1) * stk / stacks;
      glm::vec3 v = glm::vec3(
        cosf(pi * 2 * slc / slices) * radius,
        sinf(pi * 2 * slc / slices) * radius,
        height * stk / stacks
      );
      vertices.push_back(v);

      if (stk == 0 || stk == stacks) {
        normals.push_back(glm::normalize(v - glm::vec3(0, 0, height / 2)));
      } else {
        normals.push_back(glm::normalize(glm::vec3(v.x, v.y, 0)));
      }
    }
  }

  std::vector<Polygon> polygons;
  for (int stk = 0; stk < stacks; stk++) {
    for (int slc = 0; slc < slices; slc++) {
      int nstk = stk + 1;
      int nslc = (slc + 1) % slices;
      Polygon p;
      p.add(stk * slices + slc);
      p.add(stk * slices + nslc);
      p.add(nstk * slices + nslc);
      p.add(nstk * slices + slc);

      polygons.push_back(p);
    }
  }

  Polygon top;
  Polygon bottom;
  for (int i = 0; i < slices; i++) {
    top.add(stacks * slices + i);
    bottom.add(slices - i - 1);
  }

  float maxDistanceTop = sinf(pi * 2 / slices / 2) * radius2 * 2 * 1.01f;
  float minDistanceTop = maxDistanceTop * 0.1f;
  float maxDistanceBottom = sinf(pi * 2 / slices / 2) * radius1 * 2 * 1.01f;
  float minDistanceBottom = maxDistanceBottom * 0.1f;
  std::vector<Face> topFaces = dividePolygonRegular(top, vertices, normals, maxDistanceTop, minDistanceTop);
  std::vector<Face> bottomFaces = dividePolygonRegular(bottom, vertices, normals, maxDistanceBottom, minDistanceBottom);

  faces = dividePolygonArrayNaive(polygons, vertices);
  faces.insert(faces.end(), topFaces.begin(), topFaces.end());
  faces.insert(faces.end(), bottomFaces.begin(), bottomFaces.end());

  return true;
}

AnnulusLoader::AnnulusLoader(float innerRadius_, float outerRadius_, int sides_)
  : innerRadius(innerRadius_), outerRadius(outerRadius_), sides(sides_) {
}

bool
AnnulusLoader::load() {
  if (innerRadius < 0 || outerRadius < innerRadius || sides < 3) {
    return false;
  }

  for (int i = 0; i < sides; i++) {
    Polygon p;
    int i_next = (i + 1) % sides;
    p.add(i * 2);
    p.add(i * 2 + 1);
    p.add(i_next * 2 + 1);
    p.add(i_next * 2);
    polygons.push_back(p);

    float angle = pi * 2 * i / sides;
    vertices.push_back(glm::vec3(
      innerRadius * cosf(angle),
      innerRadius * sinf(angle),
      0
    ));
    normals.push_back(glm::normalize(glm::vec3(0, 0, 1)));

    vertices.push_back(glm::vec3(
      outerRadius * cosf(angle),
      outerRadius * sinf(angle),
      0
    ));
    normals.push_back(glm::normalize(glm::vec3(0, 0, 1)));
  }

  faces = dividePolygonArrayNaive(polygons, vertices);

  return true;
}

}
