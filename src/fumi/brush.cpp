// -*- C++ -*-

#include <cmath>

#include <iostream>

#include <glm/glm.hpp>

#include "common.h"

#include "brush.h"

namespace fumi {

MonomialBrush::MonomialBrush(const glm::vec3& pp, const glm::vec3& pn, float i, float o, float r)
  : pointerPos(pp), pointerNormal(pn), intensity(i), order(o), radius(r) {
}

glm::vec3
MonomialBrush::operator()(const glm::vec3& vertexPos, const glm::vec3& vertexNormal) {
  float d = glm::length(glm::cross(vertexPos - pointerPos, pointerNormal)) / radius;
  float r = clamp(1 - expf(order * logf(d)), 0, 1);
  return pointerNormal * intensity * r;
}

}
