// -*- C++ -*-

#include <cmath>
#include <cfloat>

#include <iostream>
#include <algorithm>
#include <utility>
#include <set>

#include <glm/glm.hpp>
#include <glm/gtx/intersect.hpp>

#include "common.h"
#include "userconfig.h"
#include "gldriver.h"

#include "sculptable_mesh.h"
#include "modification_guard.h"

namespace fumi {

SculptableMesh::SculptableMesh(IMeshLoader& meshLoader)
  : faces(GL_ELEMENT_ARRAY_BUFFER),
    vertices(GL_ARRAY_BUFFER),
    normals(GL_ARRAY_BUFFER),
    colors(GL_ARRAY_BUFFER),
    vertexVectorResizer(*this),
    faceVectorResizer(*this) {
  meshLoader.load();

  numVertices = meshLoader.numVertices();
  numFaces = meshLoader.numFaces();

  faces.init(meshLoader.getFaces(), meshLoader.numFaces());
  vertices.init(meshLoader.getVertices(), meshLoader.numVertices());
  normals.init(meshLoader.getNormals(), meshLoader.numVertices());
  colors.init(meshLoader.getColors(), meshLoader.numVertices());

  symmetricalFace.resize(meshLoader.numFaces(), -1);
  symmetricalVertex.resize(meshLoader.numVertices(),-1);

  vertexAllocator = std::unique_ptr<ElementAllocator>(new ElementAllocator(vertexVectorResizer));
  faceAllocator = std::unique_ptr<ElementAllocator>(new ElementAllocator(faceVectorResizer));

  calculateContainedFaces();
}

void
SculptableMesh::draw() {
  FUMI_CALLGL(glEnableVertexAttribArray(0));
  FUMI_CALLGL(glEnableVertexAttribArray(1));
  FUMI_CALLGL(glEnableVertexAttribArray(2));

  vertices.vertexAttribPointer(0);
  normals.vertexAttribPointer(1);
  colors.vertexAttribPointer(2);
  faces.drawElementsTriangles(faces.size());

  FUMI_CALLGL(glDisableVertexAttribArray(0));
  FUMI_CALLGL(glDisableVertexAttribArray(1));
  FUMI_CALLGL(glDisableVertexAttribArray(2));
}

void
SculptableMesh::calculateContainedFaces() {
  containedFaces.clear();
  containedFaces.resize(vertices.size(), std::vector<int>(0));

  for (unsigned i = 0; i < faces.size(); i++) {
    if (!faceAllocator->isUsed(i)) {
      continue;
    }

    for (int j = 0; j < 3; j++) {
      int v = faces[i].vertexIndices[j];
      containedFaces[v].push_back(i);
    }
  }
}

int
SculptableMesh::getNumFaces() {
  return faceAllocator->getNumAllocated();
}

void
SculptableMesh::setSymmetrical(bool symmetrical_) {
  symmetrical = symmetrical_;
  symmetricalFace.resize(faces.size());
  symmetricalVertex.resize(vertices.size());
}

SculptableMesh::VertexVectorResizer::VertexVectorResizer(SculptableMesh &parent_)
  : parent(&parent_) {

}

void
SculptableMesh::VertexVectorResizer::resize(int size) {
  parent->vertices.resize(size);
  parent->normals.resize(size);
  parent->colors.resize(size);
  parent->containedFaces.resize(size, std::vector<int>(0));

  if (parent->symmetrical) {
    parent->symmetricalVertex.resize(size, -1);
  }
}

int
SculptableMesh::VertexVectorResizer::size() {
  return parent->vertices.size();
}

SculptableMesh::FaceVectorResizer::FaceVectorResizer(SculptableMesh &parent_)
  : parent(&parent_) {

}

void
SculptableMesh::FaceVectorResizer::resize(int size) {
  parent->faces.resize(size);

  if (parent->symmetrical) {
    parent->symmetricalFace.resize(size, -1);
  }
}

int
SculptableMesh::FaceVectorResizer::size() {
  return parent->faces.size();
}

}
