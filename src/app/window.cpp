// -*- C++ -*-
#include <QVBoxLayout>

#include "modelingwidget.h"
#include "mainwindow.h"

#include "window.h"

Window::Window(MainWindow *mainWindow_) : mainWindow(mainWindow_) {
  modelingWidget = new ModelingWidget();

  QVBoxLayout *mainLayout = new QVBoxLayout;
  mainLayout->addWidget(modelingWidget);
  setLayout(mainLayout);
  this->layout()->setContentsMargins(0, 0, 0, 0);
}
