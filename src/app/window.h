// -*- C++ -*-

#ifndef WINDOW_H
#define WINDOW_H

#include <QWidget>

#include "modelingwidget.h"

class MainWindow;

class Window : public QWidget {
  Q_OBJECT

public:
  Window(MainWindow *mw);

private:
  ModelingWidget *modelingWidget;
  MainWindow *mainWindow;
};

#endif //WINDOW_H
