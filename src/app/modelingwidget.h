// -*- C++ -*-

#ifndef MODELINGWIDGET_H
#define MODELINGWIDGET_H

#include <glm/glm.hpp>

#include <QOpenGLWidget>
#include <QOpenGLFunctions_3_3_Core>
#include <QOpenGLShaderProgram>
#include <QTimer>

#include "fumi/common.h"
#include "fumi/geometry.h"

class ModelingWidget : public QOpenGLWidget, public QOpenGLFunctions_3_3_Core {
  Q_OBJECT

public:
  ModelingWidget(QWidget *parent = 0);

  int getNumFaces();

public slots:
  void setPointerSize(float size);
  void setSubdivisionScale(float scale);
  void setConstantPointerSize(bool isConstant);
  void setSelectedObjectSymmetry(bool symmetrical);

signals:
  void meshEdited();

protected:
  void initializeGL() Q_DECL_OVERRIDE;
  void paintGL() Q_DECL_OVERRIDE;
  void resizeGL(int width, int height) Q_DECL_OVERRIDE;
  void mouseDoubleClickEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
  void mousePressEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
  void mouseReleaseEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
  void mouseMoveEvent(QMouseEvent *event) Q_DECL_OVERRIDE;
  void wheelEvent(QWheelEvent * event) Q_DECL_OVERRIDE;
  void keyPressEvent(QKeyEvent *event) Q_DECL_OVERRIDE;

private slots:
  void tick();

private:
  void recalcPointerPosition(const QPoint& point);

  glm::mat4 projMatrix;
  glm::mat4 viewMatrix;
  glm::mat4 modelMatrix;

  GLuint shaderProgram;
  GLuint wireFrameShaderProgram;
  GLuint vertexArrayObject;

  QTimer *frameTimer;

  Qt::MouseButton clickedButton;
  QPoint clickedPoint;
  QPoint lastPoint;

  bool pointerValid;
  glm::vec3 pointerPosition;
  glm::vec3 pointerNormal;
  volatile float pointerSize;
  volatile float actualPointerSize;
  volatile bool pointerSizeIsConstant;

  volatile float subdivisionScale;

  bool usePivot;
  glm::vec3 cameraMovePivot;

  fumi::CameraInfo cameraInfo;

  bool wireFrame;
};

#endif // MODELINGWIDGET_H
