// -*- C++ -*-

#include <cstdio>
#include <string>

#include <QApplication>
#include <QDesktopWidget>
#include <QSurfaceFormat>

#include "mainwindow.h"

#include <fumi/common.h>
#include <fumi/sculptable_mesh.h>
#include <fumi/obj_loader.h>
#include <fumi/userconfig.h>

int
main(int argc, char *argv[]) {
  try {
    fumi::UserConfig::loadConfig();

    QApplication app(argc, argv);

    QSurfaceFormat fmt;
    fmt.setDepthBufferSize(24);
    fmt.setVersion(3, 3);
    fmt.setProfile(QSurfaceFormat::CoreProfile);
    QSurfaceFormat::setDefaultFormat(fmt);

    MainWindow mainWindow;
    mainWindow.show();
    return app.exec();
  } catch (std::exception& err) {
    fprintf(stderr, "%s\n", err.what());
  }
  return 0;
}
