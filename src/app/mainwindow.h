#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_selectionSizeSlider_realValueChanged(double value);
    void on_subdivisionScaleSlider_realValueChanged(double value);

    void on_modelingWidget_meshEdited();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
