// -*- C++ -*-
#include <iostream>
#include <fstream>

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include <QWheelEvent>

#include <fumi/common.h>
#include <fumi/userconfig.h>
#include <fumi/sculptable_mesh.h>
#include <fumi/static_mesh.h>
#include <fumi/obj_loader.h>
#include <fumi/primitive_loader.h>
#include <fumi/geometry.h>
#include <fumi/mesh_manipulation.h>
#include <fumi/gldriver.h>

#include <ncpplib/profiler/stopwatch.h>

#include "modelingwidget.h"

fumi::SculptableMesh* mesh;
fumi::StaticMesh* pointerMesh;

std::string readAllFile(const char *path) {
  std::ifstream ifs(path);
  if (ifs.fail()) {
    throw std::runtime_error("file not found");
  }
  std::string res((std::istreambuf_iterator<char>(ifs)), (std::istreambuf_iterator<char>()));
  return res;
}

void
compileShader(QOpenGLFunctions_3_3_Core *p, GLuint shader, const std::string& src) {
  GLint res = GL_FALSE;
  const char *src_p = src.c_str();
  p->glShaderSource(shader, 1, &src_p, 0);
  p->glCompileShader(shader);

  p->glGetShaderiv(shader, GL_COMPILE_STATUS, &res);
  if (res == GL_FALSE) {
    throw std::runtime_error("Shader compilation error");
  }
}

GLuint
loadShaderProgram(QOpenGLFunctions_3_3_Core *p, const char *vertexShaderPath, const char *fragmentShaderPath) {
  GLuint vertexShader = p->glCreateShader(GL_VERTEX_SHADER);
  GLuint fragmentShader = p->glCreateShader(GL_FRAGMENT_SHADER);

  compileShader(p, vertexShader, readAllFile(vertexShaderPath));
  compileShader(p, fragmentShader, readAllFile(fragmentShaderPath));

  GLuint shaderProgram = p->glCreateProgram();
  p->glAttachShader(shaderProgram, vertexShader);
  p->glAttachShader(shaderProgram, fragmentShader);
  p->glLinkProgram(shaderProgram);

  GLint res = GL_FALSE;
  p->glGetProgramiv(shaderProgram, GL_LINK_STATUS, &res);
  if (res == GL_FALSE) {
    throw std::runtime_error("Shader linkage error");
  }
  p->glDeleteShader(vertexShader);
  p->glDeleteShader(fragmentShader);

  return shaderProgram;
}

// For debugging

struct {
  GLuint vertices;
  GLuint colors;
} lineBuffer;

GLuint lineProgram;

const float lineVertexData[] {
  0.0f, 0.0f, 0.0f,
  1.0f, 1.0f, 1.0f
};

const float lineColorData[] {
  1.0f, 0.5f, 0.5f,
  0.5f, 0.5f, 1.0f
};

static void
initDebugLine(QOpenGLFunctions_3_3_Core *p) {
  p->glGenBuffers(2, reinterpret_cast<GLuint *>(&lineBuffer));

  p->glBindBuffer(GL_ARRAY_BUFFER, lineBuffer.vertices);
  p->glBufferData(GL_ARRAY_BUFFER, sizeof(lineVertexData), lineVertexData, GL_STATIC_DRAW);

  p->glBindBuffer(GL_ARRAY_BUFFER, lineBuffer.colors);
  p->glBufferData(GL_ARRAY_BUFFER, sizeof(lineColorData), lineColorData, GL_STATIC_DRAW);

  lineProgram = loadShaderProgram(p, fumi::UserConfig::debugVertexShaderPath.c_str(), fumi::UserConfig::debugFragmentShaderPath.c_str());
}


static void
drawDebugLine(QOpenGLFunctions_3_3_Core *p, const glm::mat4& proj, const glm::mat4& view, const glm::vec3& v0, const glm::vec3& v1) {
  glm::mat4 model = glm::translate(glm::scale(glm::mat4(1), v1 - v0), v0);

  p->glUseProgram(lineProgram);

  GLuint mvpId = p->glGetUniformLocation(lineProgram, "mvpMatrix");
  p->glUniformMatrix4fv(mvpId, 1, GL_FALSE, glm::value_ptr(proj * view * model));
  glm::vec3 ld(-1, 0, 0);

  p->glEnableVertexAttribArray(0);
  p->glEnableVertexAttribArray(1);

  p->glBindBuffer(GL_ARRAY_BUFFER, lineBuffer.vertices);
  p->glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, 0);

  p->glBindBuffer(GL_ARRAY_BUFFER, lineBuffer.colors);
  p->glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, 0);

  p->glDrawArrays(GL_LINES, 0, 6);

  p->glDisableVertexAttribArray(0);
  p->glDisableVertexAttribArray(1);
}

ModelingWidget::ModelingWidget(QWidget *parent)
  : QOpenGLWidget(parent), frameTimer(new QTimer(this)), clickedButton(Qt::NoButton) {
  setMouseTracking(true);

  connect(frameTimer, SIGNAL(timeout()), this, SLOT(tick()));
  frameTimer->setInterval(16);
  frameTimer->setSingleShot(true);
  cameraInfo.cameraPosition = glm::vec3(0, 0, 3);
  cameraInfo.lookAtPosition = glm::vec3(0, 0, 0);
  cameraInfo.upDirection = glm::vec3(0, 1, 0);
  cameraInfo.fovY = 45.0f;
  cameraInfo.zNear = 0.01f;
  cameraInfo.zFar = 1000.0f;

  pointerSize = 0.2;
  actualPointerSize = pointerSize;
  pointerSizeIsConstant = true;

  subdivisionScale = 0.5f;

  setFocusPolicy(Qt::StrongFocus);
}

int
ModelingWidget::getNumFaces() {
  return mesh->getNumFaces();
}

void
ModelingWidget::recalcPointerPosition(const QPoint& point) {
  glm::vec3 v = fumi::proj2WorldDirection(cameraInfo, point.x(), point.y());
  glm::mat4 modelMatrixInverse = glm::inverse(modelMatrix);

  glm::vec3 cameraPos = fumi::mapPos(modelMatrixInverse, cameraInfo.cameraPosition);
  glm::vec3 cameraDir = fumi::mapDir(modelMatrixInverse, v);
  glm::vec3 pos;
  glm::vec3 normal;
  if (fumi::intersection(*mesh, cameraPos, cameraDir, pos, normal) >= 0) {
    pointerPosition = fumi::mapPos(modelMatrix, pos);
    pointerNormal = fumi::mapDir(modelMatrix, normal);
    pointerPosition += pointerNormal * 0.1f;
    pointerValid = true;

    if (!pointerSizeIsConstant) {
      actualPointerSize = pointerSize * glm::length(pointerPosition - cameraInfo.cameraPosition);
    }
  } else {
    pointerPosition = glm::vec3(0, 0, 0);
    pointerValid = false;
  }
}

void
ModelingWidget::initializeGL() {
  initializeOpenGLFunctions();
  fumi::gldriver::initGLDriver(static_cast<QOpenGLFunctions_3_3_Core *>(this));

  glGenVertexArrays(1, &vertexArrayObject);
  glBindVertexArray(vertexArrayObject);

  //CylinderLoader objLoader(10.0f, 10.0f, 10.0f, 10, 10);
  fumi::SphereLoader objLoader(10.0f, 4);
  mesh = new fumi::SculptableMesh(objLoader);
  fumi::AnnulusLoader pointerLoader(0.9f, 1.0f, 30);
  pointerMesh = new fumi::StaticMesh(pointerLoader);

  shaderProgram = loadShaderProgram(static_cast<QOpenGLFunctions_3_3_Core *>(this),
    fumi::UserConfig::vertexShaderPath.c_str(),
    fumi::UserConfig::fragmentShaderPath.c_str());
  wireFrameShaderProgram = loadShaderProgram(static_cast<QOpenGLFunctions_3_3_Core *>(this),
    fumi::UserConfig::vertexShaderPath.c_str(),
    fumi::UserConfig::solidFragmentShaderPath.c_str());
  glEnable(GL_DEPTH_TEST);

  fumi::Color& cc = fumi::UserConfig::clearColor;
  glClearColor(cc.r, cc.g, cc.b, cc.a);

  frameTimer->start();

  initDebugLine(static_cast<QOpenGLFunctions_3_3_Core *>(this));
}

void
ModelingWidget::paintGL() {
  // Select correct buffer for this context.
  this->glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
  this->glUseProgram(shaderProgram);
  // Clear the buffers for new frame.

  glm::mat4 view = glm::lookAt(
    cameraInfo.cameraPosition,
    cameraInfo.lookAtPosition,
    cameraInfo.upDirection);
  viewMatrix = view;

  static float rotation = 15.0f;
  //rotation += 1.0f;
  glm::mat4 model = glm::rotate(glm::mat4(1.0f), fumi::deg2Rad(rotation), glm::vec3(1, 0.5, 0));
  modelMatrix = model;

  GLuint mvpId = this->glGetUniformLocation(shaderProgram, "mvpMatrix");
  this->glUniformMatrix4fv(mvpId, 1, GL_FALSE, glm::value_ptr(projMatrix * view * model));
  GLuint modelId = this->glGetUniformLocation(shaderProgram, "mMatrix");
  this->glUniformMatrix4fv(modelId, 1, GL_FALSE, glm::value_ptr(model));
  GLuint viewId = this->glGetUniformLocation(shaderProgram, "vMatrix");
  this->glUniformMatrix4fv(viewId, 1, GL_FALSE, glm::value_ptr(view));
  glm::vec3 ld(-1, 0, 0);
  GLuint lId = this->glGetUniformLocation(shaderProgram, "lightDirection_worldspace");
  this->glUniform3f(lId, ld.x, ld.y, ld.z);

  // Draw cube geometry
  this->glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
  mesh->draw();

  if (pointerValid) {
    this->glUseProgram(wireFrameShaderProgram);
    GLuint mvpId = this->glGetUniformLocation(wireFrameShaderProgram, "mvpMatrix");
    GLuint modelId = this->glGetUniformLocation(wireFrameShaderProgram, "mMatrix");
    GLuint viewId = this->glGetUniformLocation(wireFrameShaderProgram, "vMatrix");
    GLuint colorId = this->glGetUniformLocation(wireFrameShaderProgram, "solidColor");
    glm::mat4 pointerRot = fumi::getRotationMatrix(glm::vec3(0, 0, 1), pointerNormal);
    glm::mat4 pointerScale = glm::scale(glm::mat4(1), glm::vec3(actualPointerSize, actualPointerSize, actualPointerSize));
    glm::mat4 pointerModel = glm::translate(glm::mat4(1), pointerPosition) * pointerRot * pointerScale;

    this->glUniformMatrix4fv(mvpId, 1, GL_FALSE, glm::value_ptr(projMatrix * view * pointerModel));
    this->glUniformMatrix4fv(modelId, 1, GL_FALSE, glm::value_ptr(pointerModel));
    this->glUniformMatrix4fv(viewId, 1, GL_FALSE, glm::value_ptr(view));
    this->glUniform3f(colorId, 1.0, 0, 0);

    pointerMesh->draw();
  }

  if (wireFrame) {
    this->glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    this->glUseProgram(wireFrameShaderProgram);

    GLuint mvpId = this->glGetUniformLocation(wireFrameShaderProgram, "mvpMatrix");
    this->glUniformMatrix4fv(mvpId, 1, GL_FALSE, glm::value_ptr(projMatrix * view * model));
    GLuint modelId = this->glGetUniformLocation(wireFrameShaderProgram, "mMatrix");
    this->glUniformMatrix4fv(modelId, 1, GL_FALSE, glm::value_ptr(model));
    GLuint viewId = this->glGetUniformLocation(wireFrameShaderProgram, "vMatrix");
    this->glUniformMatrix4fv(viewId, 1, GL_FALSE, glm::value_ptr(view));
    GLuint colorId = this->glGetUniformLocation(wireFrameShaderProgram, "solidColor");
    this->glUniform3f(colorId, 1.0, 0, 0);

    mesh->draw();
  }
}

void
ModelingWidget::resizeGL(int width, int height) {
  cameraInfo.aspectRatio = (float)width / height;
  projMatrix = glm::perspective(
    cameraInfo.fovY, cameraInfo.aspectRatio,
    cameraInfo.zNear, cameraInfo.zFar);
  cameraInfo.viewportWidth = width;
  cameraInfo.viewportHeight = height;
}

void
ModelingWidget::tick() {
  if (pointerValid && clickedButton == Qt::LeftButton) {
    glm::mat4 modelMatrixInverse = glm::inverse(modelMatrix);
    glm::vec3 pointerPosMS = fumi::mapPos(modelMatrixInverse, pointerPosition);
    glm::vec3 pointerDirMS = fumi::mapDir(modelMatrixInverse, -pointerNormal);
    fumi::MonomialBrush brush(pointerPosMS, -pointerDirMS, 0.01f, 2, actualPointerSize);
    ncpplib::profiler::StopWatch<>::measureExecution("divide(): ", [&] { fumi::divide(*mesh, pointerPosMS, pointerDirMS, actualPointerSize, actualPointerSize * subdivisionScale, 1.3f); });
    ncpplib::profiler::StopWatch<>::measureExecution("sculpt(): ", [&] { fumi::sculpt(*mesh, brush, pointerPosMS, pointerDirMS, actualPointerSize); });
    ncpplib::profiler::StopWatch<>::measureExecution("recalc", [&] { recalcPointerPosition(lastPoint); });
    emit meshEdited();
  }
  update();
  frameTimer->start();
}

void
ModelingWidget::mouseDoubleClickEvent(QMouseEvent *event) {
  clickedButton = event->button();
  // The pointer position is already calculated in ModelingWidget::mousePressEvent

  // lastPoint = clickedPoint = event->pos();
  // recalcPointerPosition(clickedPoint);

  if (clickedButton == Qt::RightButton) {
    if (pointerValid) {
      cameraInfo = fumi::moveCameraToFrontOfPointer(cameraInfo, pointerPosition, pointerNormal, fumi::UserConfig::manipMoveToFrontOfPointerDistance);
    }
  }
}

void
ModelingWidget::mousePressEvent(QMouseEvent *event) {
  clickedButton = event->button();
  lastPoint = clickedPoint = event->pos();

  recalcPointerPosition(clickedPoint);

  if (clickedButton == Qt::RightButton) {
    if (pointerValid) {
      cameraMovePivot = pointerPosition;
      usePivot = true;
    } else {
      usePivot = false;
    }
  } else if (clickedButton == Qt::LeftButton) {
    if (pointerValid) {
      glm::mat4 modelMatrixInverse = glm::inverse(modelMatrix);
      glm::vec3 pointerPosMS = fumi::mapPos(modelMatrixInverse, pointerPosition);
      glm::vec3 pointerDirMS = fumi::mapDir(modelMatrixInverse, -pointerNormal);

      fumi::divide(*mesh, pointerPosMS, pointerDirMS, actualPointerSize, actualPointerSize * subdivisionScale, 1.3f);
    }
  }

  event->accept();
}

void
ModelingWidget::mouseReleaseEvent(QMouseEvent *event) {
  clickedButton = Qt::NoButton;
  lastPoint = event->pos();
  recalcPointerPosition(clickedPoint);

  event->accept();
}

void
ModelingWidget::mouseMoveEvent(QMouseEvent *event) {
  if (clickedButton == Qt::RightButton) {
    QPoint currPos = event->pos();

    int dx = currPos.x() - lastPoint.x();
    int dy = currPos.y() - lastPoint.y();

    if (usePivot) {
      cameraInfo = fumi::moveCameraAroundPivot(cameraInfo, cameraMovePivot, dx, dy, fumi::UserConfig::manipPivotRotateFactor);
    } else {
      cameraInfo = fumi::moveCameraAroundPivot(cameraInfo, cameraInfo.lookAtPosition, dx, dy, fumi::UserConfig::manipNoPivotRotateFactor);
    }

    lastPoint = currPos;
  } else {
    lastPoint = clickedPoint = event->pos();

    recalcPointerPosition(clickedPoint);
  }

  event->accept();
}

void
ModelingWidget::wheelEvent(QWheelEvent * event) {
  int dyPixels = event->pixelDelta().isNull() ? 0 : event->pixelDelta().y();
  int dyDegrees = event->angleDelta().isNull() ? 0 : event->angleDelta().y();

  glm::vec3 d = glm::normalize(cameraInfo.lookAtPosition - cameraInfo.cameraPosition);
  glm::vec3 move;

  if (dyPixels != 0) {
    move = d * fumi::UserConfig::manipMoveForwardFactor * (float)dyPixels;
  } else {
    move = d * fumi::UserConfig::manipMoveForwardFactor * (float)dyDegrees;
  }

  cameraInfo.cameraPosition += move;
  lastPoint = event->pos();
  recalcPointerPosition(lastPoint);

  event->accept();
}

void
ModelingWidget::keyPressEvent(QKeyEvent *event) {
  switch (event->key()) {
    case Qt::Key_W:
      wireFrame = !wireFrame;
      break;
  }
}

void
ModelingWidget::setPointerSize(float size) {
  pointerSize = size;
  if (pointerSizeIsConstant) {
    actualPointerSize = pointerSize;
  }
}

void
ModelingWidget::setSubdivisionScale(float scale) {
  subdivisionScale = scale;
}

void
ModelingWidget::setConstantPointerSize(bool isConstant) {
  pointerSizeIsConstant = isConstant;
}

void
ModelingWidget::setSelectedObjectSymmetry(bool symmetrical) {
  symmetrize(*mesh);
}
