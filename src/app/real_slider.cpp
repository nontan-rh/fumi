// -*- C++ -*-

#include <cmath>

#include "real_slider.h"

RealSlider::RealSlider(QWidget *parent) : QSlider(parent), mode(Linear) {
 connect(this, SIGNAL(valueChanged(int)), this, SLOT(notifyValueChanged(int)));
}

void
RealSlider::setMode(StepMode mode_) {
  mode = mode_;
}

void
RealSlider::setNumSteps(int numSteps_) {
  numSteps = numSteps_;
  QSlider::setRange(0, numSteps);
}

void
RealSlider::setMinimumValue(double minimumValue_) {
  if (minimumValue != minimumValue_) {
    notifyValueChanged(QSlider::sliderPosition());
  }
  minimumValue = minimumValue_;
}

void
RealSlider::setMaximumValue(double maximumValue_) {
  if (maximumValue != maximumValue_) {
    notifyValueChanged(QSlider::sliderPosition());
  }
  maximumValue = maximumValue_;
}

void
RealSlider::setValue(double value) {
  QSlider::setSliderPosition(calculatePosition(value));
}

RealSlider::StepMode
RealSlider::getMode() {
  return mode;
}

int
RealSlider::getNumSteps() {
  return numSteps;
}

double
RealSlider::getMinimumValue() {
  return minimumValue;
}

double
RealSlider::getMaximumValue() {
  return maximumValue;
}

double
RealSlider::getValue() {
  return calculateValue(QSlider::sliderPosition());
}

void
RealSlider::notifyValueChanged(int value) {
  emit realValueChanged(calculateValue(value));
}

double
RealSlider::calculateValue(int value) {
  double r = static_cast<double>(value) / numSteps;
  double realValue;
  switch (mode) {
    case Linear:
      realValue = minimumValue + (maximumValue - minimumValue) * r;
      break;
    case Exponential:
      realValue = minimumValue * exp(r * log(maximumValue / minimumValue));
      break;
    case InversePropotional: {
      double minInv = 1 / minimumValue;
      double maxInv = 1 / maximumValue;
      realValue = 1 / ((maxInv - minInv) * r + minInv);
      break;
    }
    default:
      throw std::runtime_error("Unknown mode");
  }
  return realValue;
}

int
RealSlider::calculatePosition(double realValue) {
  double r;
  switch (mode) {
    case Linear:
      r = (realValue - minimumValue) / (maximumValue - minimumValue);
      break;
    case Exponential:
      r = log(realValue / minimumValue) / log(maximumValue / minimumValue);
      break;
    case InversePropotional: {
      double valueInv = 1 / realValue;
      double minInv = 1 / minimumValue;
      double maxInv = 1 / maximumValue;
      r = (valueInv - minInv) / (maxInv - minInv);
      break;
    }
    default:
      throw std::runtime_error("Unknown mode");
  }
  return round(r * numSteps);
}
