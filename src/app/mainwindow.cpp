#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
  ui->setupUi(this);

  QObject::connect(
    ui->modelingWidget, SIGNAL(meshEdited()),
    this, SLOT(on_modelingWidget_meshEdited()));

  QObject::connect(
    ui->selectionSizeSlider, SIGNAL(realValueChanged(double)),
    this, SLOT(on_selectionSizeSlider_realValueChanged(double)));

  QObject::connect(
    ui->subdivisionScaleSlider, SIGNAL(realValueChanged(double)),
    this, SLOT(on_subdivisionScaleSlider_realValueChanged(double)));

  QObject::connect(
    ui->constantPointerSizeToggle, SIGNAL(toggled(bool)),
    ui->modelingWidget, SLOT(setConstantPointerSize(bool)));

  QObject::connect(
    ui->symmetryCheckBox, SIGNAL(toggled(bool)),
    ui->modelingWidget, SLOT(setSelectedObjectSymmetry(bool)));

  ui->selectionSizeSlider->setMinimumValue(0.01);
  ui->selectionSizeSlider->setMaximumValue(0.50);
  ui->selectionSizeSlider->setNumSteps(100);
  ui->selectionSizeSlider->setValue(0.25);

  ui->subdivisionScaleSlider->setMinimumValue(0.1);
  ui->subdivisionScaleSlider->setMaximumValue(10.0);
  ui->subdivisionScaleSlider->setNumSteps(100);
  ui->subdivisionScaleSlider->setValue(0.25);
  ui->subdivisionScaleSlider->setMode(RealSlider::Exponential);

  ui->constantPointerSizeToggle->setChecked(false);
}

MainWindow::~MainWindow()
{
  delete ui;
}

void
MainWindow::on_modelingWidget_meshEdited()
{
  QString s = QString("%1 faces").arg(ui->modelingWidget->getNumFaces());
  ui->statusbar->showMessage(s);
}

void
MainWindow::on_selectionSizeSlider_realValueChanged(double value) {
  ui->modelingWidget->setPointerSize(value);
  ui->sizeLabel->setText(QString("%1").arg(value, 0, 'f', 2));
}

void
MainWindow::on_subdivisionScaleSlider_realValueChanged(double value) {
  ui->modelingWidget->setSubdivisionScale(value);
  ui->scaleLabel->setText(QString("%1").arg(value, 0, 'f', 2));
}
