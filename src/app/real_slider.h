// -*- C++ -*-

#ifndef REAL_SLIDER_H
#define REAL_SLIDER_H

#include <QSlider>

class RealSlider : public QSlider {
  Q_OBJECT

  RealSlider(const RealSlider& o);
  RealSlider& operator=(const RealSlider& rht);

public:
  RealSlider(QWidget *parent = 0);

  enum StepMode {
    Linear,
    Exponential,
    InversePropotional,
  };

  void setMode(StepMode mode);
  void setNumSteps(int numSteps);
  void setMinimumValue(double minimumValue);
  void setMaximumValue(double maximumValue);
  void setValue(double value);

  StepMode getMode();
  int getNumSteps();
  double getMinimumValue();
  double getMaximumValue();
  double getValue();

signals:
  void realValueChanged(double value);

public slots:
  void notifyValueChanged(int value);

private:
  int numSteps;
  double minimumValue;
  double maximumValue;
  StepMode mode;

  double calculateValue(int value);
  int calculatePosition(double realValue);
};

#endif // REAL_SLIDER_H
