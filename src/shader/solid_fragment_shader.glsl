#version 330 core

in vec3 fragmentColor;
uniform vec3 solidColor;

out vec3 color;

void main()
{
	color = solidColor;
}
