#version 330 core

in vec3 fragmentColor;
in vec3 vertexNormal_cameraspace;
in vec3 eyeDirection_cameraspace;
in vec3 lightDirection_cameraspace;

out vec3 color;

void main()
{
	vec3 lightColor = vec3(1, 1, 1);
	vec3 materialAmbientColor = vec3(0.3, 0.3, 0.3) * fragmentColor;
	vec3 materialSpecularColor = vec3(0.3, 0.3, 0.3);

	vec3 n = normalize(vertexNormal_cameraspace);
	vec3 l = normalize(lightDirection_cameraspace);
	vec3 e = normalize(eyeDirection_cameraspace);
	vec3 r = reflect(-l, n);

	float theta = clamp(dot(n, l), 0, 1);
	float alpha = clamp(dot(e, r), 0, 1);

	color = materialAmbientColor
	  + fragmentColor * lightColor * theta
		+ materialSpecularColor * lightColor * pow(alpha, 5);
}
