#version 330 core

layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec3 vertexNormal_modelspace;
layout(location = 2) in vec3 vertexColor;

out vec3 fragmentColor;
out vec3 vertexNormal_cameraspace;
out vec3 eyeDirection_cameraspace;
out vec3 lightDirection_cameraspace;

uniform mat4 mvpMatrix;
uniform mat4 vMatrix;
uniform mat4 mMatrix;
uniform vec3 lightDirection_worldspace;

void main(){
  gl_Position = mvpMatrix * vec4(vertexPosition_modelspace, 1.0);
  fragmentColor = vertexColor;

  vec3 vertexPosition_cameraspace = (vMatrix * mMatrix * vec4(vertexPosition_modelspace, 1.0)).xyz;
  eyeDirection_cameraspace = vec3(0, 0, 0) - vertexPosition_cameraspace;

  lightDirection_cameraspace = (vMatrix * vec4(lightDirection_worldspace, 0)).xyz;

  vertexNormal_cameraspace = (vMatrix * mMatrix * vec4(vertexNormal_modelspace, 0)).xyz;
}
