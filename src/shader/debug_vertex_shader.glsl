#version 330 core

layout(location = 0) in vec3 vertexPosition_modelspace;
layout(location = 1) in vec3 vertexColor;

out vec3 fragmentColor;

uniform mat4 mvpMatrix;

void main(){
  gl_Position = mvpMatrix * vec4(vertexPosition_modelspace, 1.0);
  fragmentColor = vertexColor;
}
