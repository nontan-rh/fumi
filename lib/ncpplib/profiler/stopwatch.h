// -*- C++ -*-

#ifndef STOPWATCH_H
#define STOPWATCH_H

#include <iostream>
#include <chrono>
#include <type_traits>

namespace ncpplib {
namespace profiler {
namespace detail {

template<typename Ret, typename TimeT>
class StopWatchAux
{
public:
  template<typename Function, typename... Args>
  static typename std::result_of<Function(Args...)>::type
  f(const std::string& message, Function&& f, Args&&... args)
  {
    auto start = std::chrono::high_resolution_clock::now();
    auto result = f(std::forward(args)...);
    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<TimeT>(end - start);
    std::cout << message << duration.count() << std::endl;
    return result;
  }
};

template<typename TimeT>
class StopWatchAux<void, TimeT>
{
public:
  template<typename Function, typename... Args>
  static void
  f(const std::string& message, Function&& f, Args&&... args)
  {
    auto start = std::chrono::high_resolution_clock::now();
    f(std::forward(args)...);
    auto end = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<TimeT>(end - start);
    std::cout << message << duration.count() << std::endl;
  }
};

}

template<typename TimeT = std::chrono::microseconds>
class StopWatch
{
public:
  template<typename Function, typename... Args>
  static typename std::result_of<Function(Args...)>::type
  measureExecution(const std::string& message, Function&& func, Args&&... args)
  {
    using Ret = typename std::result_of<Function(Args...)>::type;
    return detail::StopWatchAux<Ret, TimeT>::f(message, func, std::forward(args)...);
  }
};

}
}

#endif // STOPWATCH_H
