// -*- C++ -*-

#ifndef NCPPLIB_ALGORITHM_H_
#define NCPPLIB_ALGORITHM_H_

namespace ncpplib {
namespace algorithm {

template<typename T, typename Predicate>
inline T
findIfIndex(T begin, T end, Predicate pred) {
  for (int i = begin; i < end; i++) {
    if (pred(i)) {
      return i;
    }
  }
  return end;
}

}
}

#endif // NCPPLIB_ALGORITHM_H_
