// -*- C++ -*-

#ifndef NCPPLIB_MATH_OPS_H_
#define NCPPLIB_MATH_OPS_H_

#include <algorithm>

#include <cfloat>
#include <cmath>

namespace ncpplib {
namespace math {

inline bool
isZero(float x) {
  return std::abs(x) <= FLT_EPSILON;
}

inline bool
isZero(double x) {
  return std::abs(x) <= DBL_EPSILON;
}

inline bool
eq(float x, float y, float epsilon = FLT_EPSILON) {
  float diff = std::abs(x - y);
  float error = std::max(std::abs(x), std::abs(y)) * epsilon;
  return diff <= error;
}

inline bool
eq(double x, double y, double epsilon = DBL_EPSILON) {
  double diff = std::abs(x - y);
  double error = std::max(std::abs(x), std::abs(y)) * epsilon;
  return diff <= error;
}

inline bool
isPlus(float x) {
  return x > FLT_EPSILON;
}

inline bool
isPlus(double x) {
  return x > DBL_EPSILON;
}

inline bool
isMinus(float x) {
  return x < -FLT_EPSILON;
}

inline bool
isMinus(double x) {
  return x < -DBL_EPSILON;
}

}
}

#endif // NCPPLIB_MATH_OPS_H_
