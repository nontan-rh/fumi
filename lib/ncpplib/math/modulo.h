// -*- C++ -*-

#ifndef NCPPLIB_MATH_MODULO_H_
#define NCPPLIB_MATH_MODULO_H_

namespace ncpplib {
namespace math {
namespace modulo {

template<typename T>
inline T
abs(T value, T mod) {
  T m = value % mod;
  return m < 0 ? mod + m : m;
}

template<typename T>
inline T
uIncr(T value, T mod, T del = 1) {
  return ncpplib::math::modulo::abs(value + del, mod);
}

template<typename T>
inline T
uDecr(T value, T mod, T del = 1) {
  return ncpplib::math::modulo::abs(value - del, mod);
}

}
}
}

#endif // NCPPLIB_MATH_MODULO_H_
