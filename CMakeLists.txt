cmake_minimum_required(VERSION 2.8.3)

project(fumi)

find_package(Qt5Widgets)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_AUTOUIC ON)
set(CMAKE_AUTOMOC ON)

include_directories(${Qt5Widgets_INCLUDE_DIRS} ./lib ./src)
add_definitions(${Qt5Widgets_DEFINITIONS})

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${Qt5Widgets_EXECUTABLE_COMPILE_FLAGS}")

FILE(GLOB APP_SOURCES src/app/*.cpp)
FILE(GLOB FUMI_SOURCES src/fumi/*.cpp)

add_definitions(-DFUMI_USE_QT)
add_library(fumicore SHARED ${FUMI_SOURCES})

add_executable(fumi ${APP_SOURCES})
target_link_libraries(fumi Qt5::Widgets fumicore)

file(COPY src/shader DESTINATION ${CMAKE_BINARY_DIR})
file(COPY src/config.json DESTINATION ${CMAKE_BINARY_DIR})
