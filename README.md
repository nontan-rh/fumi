Qt5が必要

ビルド

```
#!bash


$ cmake .. -DQt5Widgets_DIR=<qmake-library-directory>/cmake/Qt5Widgets/ -DCMAKE_BUILD_TYPE=Release
$ make
```

実行

```
#!bash

$ ./fumi
```

操作方法

* マウスホイール: 視点前後移動

* 右クリックでドラッグ: 視点移動

* 右ダブルクリック: クリックした点に注目

* 左クリック長押し: メッシュを盛り上がらせる